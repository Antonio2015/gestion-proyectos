
CREATE TABLE public.estado (
                id_estado INTEGER NOT NULL,
                nombre_estado VARCHAR NOT NULL,
                CONSTRAINT id_estado PRIMARY KEY (id_estado)
);


CREATE SEQUENCE public.backlog_id_backlog_seq_1;

CREATE TABLE public.backlog (
                id_backlog INTEGER NOT NULL DEFAULT nextval('public.backlog_id_backlog_seq_1'),
                nombre_backlog VARCHAR NOT NULL,
                prioridad INTEGER NOT NULL,
                sprint INTEGER NOT NULL,
                CONSTRAINT id_backlog_pk PRIMARY KEY (id_backlog)
);


ALTER SEQUENCE public.backlog_id_backlog_seq_1 OWNED BY public.backlog.id_backlog;

CREATE SEQUENCE public.sprint_id_sprint_seq;

CREATE TABLE public.sprint (
                id_sprint INTEGER NOT NULL DEFAULT nextval('public.sprint_id_sprint_seq'),
                id_estado INTEGER NOT NULL,
                id_backlog INTEGER NOT NULL,
                nombre VARCHAR NOT NULL,
                duracion VARCHAR NOT NULL,
                fecha_inicio DATE NOT NULL,
                fecha_fin DATE NOT NULL,
                observacion VARCHAR NOT NULL,
                CONSTRAINT id_sprint_pk PRIMARY KEY (id_sprint)
);


ALTER SEQUENCE public.sprint_id_sprint_seq OWNED BY public.sprint.id_sprint;

CREATE SEQUENCE public.iid_rol_seq;

CREATE TABLE public.roles (
                id_rol INTEGER NOT NULL DEFAULT nextval('public.iid_rol_seq'),
                nombre_rol VARCHAR NOT NULL,
                descripcion_rol VARCHAR NOT NULL,
                CONSTRAINT id_rol_pk PRIMARY KEY (id_rol)
);


ALTER SEQUENCE public.iid_rol_seq OWNED BY public.roles.id_rol;

CREATE SEQUENCE public.permiso_id_permiso_seq;

CREATE TABLE public.permiso (
                id_permiso INTEGER NOT NULL DEFAULT nextval('public.permiso_id_permiso_seq'),
                id_rol INTEGER NOT NULL,
                nombre_permiso VARCHAR NOT NULL,
                CONSTRAINT id_permiso_pk PRIMARY KEY (id_permiso)
);


ALTER SEQUENCE public.permiso_id_permiso_seq OWNED BY public.permiso.id_permiso;

CREATE SEQUENCE public.iid_usuario_seq;

CREATE TABLE public.usuario (
                id_usuario INTEGER NOT NULL DEFAULT nextval('public.iid_usuario_seq'),
                id_rol INTEGER NOT NULL,
                nombre_usuario VARCHAR NOT NULL,
                password VARCHAR NOT NULL,
                cedula VARCHAR NOT NULL,
                nombre VARCHAR NOT NULL,
                apellido VARCHAR NOT NULL,
                direccionn VARCHAR NOT NULL,
                telefono VARCHAR NOT NULL,
                email VARCHAR NOT NULL,
                CONSTRAINT id_usuario_pk PRIMARY KEY (id_usuario)
);


ALTER SEQUENCE public.iid_usuario_seq OWNED BY public.usuario.id_usuario;

CREATE SEQUENCE public.equipo_id_equipo_seq;

CREATE TABLE public.equipo (
                id_equipo INTEGER NOT NULL DEFAULT nextval('public.equipo_id_equipo_seq'),
                id_usuario INTEGER NOT NULL,
                nombre_equipo VARCHAR NOT NULL,
                rol_equipo VARCHAR NOT NULL,
                CONSTRAINT id_equipo_pk PRIMARY KEY (id_equipo)
);


ALTER SEQUENCE public.equipo_id_equipo_seq OWNED BY public.equipo.id_equipo;

CREATE SEQUENCE public.proyecto_id_proyecto_seq;

CREATE TABLE public.proyecto (
                id_proyecto INTEGER NOT NULL DEFAULT nextval('public.proyecto_id_proyecto_seq'),
                id_estado INTEGER NOT NULL,
                id_backlog INTEGER NOT NULL,
                id_equipo INTEGER NOT NULL,
                nombre_proyecto VARCHAR NOT NULL,
                fecha_fin DATE NOT NULL,
                cantidad_sprint VARCHAR NOT NULL,
                estado_proyecto VARCHAR NOT NULL,
                fecha_fin_estimada DATE NOT NULL,
                anho_proyecto VARCHAR NOT NULL,
                CONSTRAINT id_proyecto_pk PRIMARY KEY (id_proyecto)
);
COMMENT ON TABLE public.proyecto IS 'adfasdfsadfsadas';


ALTER SEQUENCE public.proyecto_id_proyecto_seq OWNED BY public.proyecto.id_proyecto;

CREATE SEQUENCE public.userstory_id_userstory_sequence;

CREATE TABLE public.userstory (
                id_userstory INTEGER NOT NULL DEFAULT nextval('public.userstory_id_userstory_sequence'),
                id_proyecto INTEGER NOT NULL,
                id_usuario INTEGER NOT NULL,
                id_sprint INTEGER NOT NULL,
                nombre_largo VARCHAR NOT NULL,
                tiempo_asignado VARCHAR NOT NULL,
                descripcion VARCHAR NOT NULL,
                nombre_largo_1 VARCHAR NOT NULL,
                valor_negocio VARCHAR NOT NULL,
                estado INTEGER NOT NULL,
                fecha_inicio DATE NOT NULL,
                fecha_fin VARCHAR NOT NULL,
                CONSTRAINT id_userstory PRIMARY KEY (id_userstory)
);


ALTER SEQUENCE public.userstory_id_userstory_sequence OWNED BY public.userstory.id_userstory;

CREATE SEQUENCE public.reuniones_id_reunion_seq;

CREATE TABLE public.reuniones (
                id_reunion INTEGER NOT NULL DEFAULT nextval('public.reuniones_id_reunion_seq'),
                id_equipo INTEGER NOT NULL,
                fecha_reunion DATE NOT NULL,
                observacion VARCHAR NOT NULL,
                CONSTRAINT id_reuniones_pk PRIMARY KEY (id_reunion)
);


ALTER SEQUENCE public.reuniones_id_reunion_seq OWNED BY public.reuniones.id_reunion;

ALTER TABLE public.proyecto ADD CONSTRAINT estado_proyecto_fk
FOREIGN KEY (id_estado)
REFERENCES public.estado (id_estado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.sprint ADD CONSTRAINT estado_sprint_fk
FOREIGN KEY (id_estado)
REFERENCES public.estado (id_estado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.sprint ADD CONSTRAINT flujo_sprint_fk
FOREIGN KEY (id_backlog)
REFERENCES public.backlog (id_backlog)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.proyecto ADD CONSTRAINT backlog_proyecto_fk
FOREIGN KEY (id_backlog)
REFERENCES public.backlog (id_backlog)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.userstory ADD CONSTRAINT sprint_userstory_fk
FOREIGN KEY (id_sprint)
REFERENCES public.sprint (id_sprint)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.permiso ADD CONSTRAINT roles_permiso_fk
FOREIGN KEY (id_rol)
REFERENCES public.roles (id_rol)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuario ADD CONSTRAINT roles_usuario_fk
FOREIGN KEY (id_rol)
REFERENCES public.roles (id_rol)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.equipo ADD CONSTRAINT usuario_equipo_fk
FOREIGN KEY (id_usuario)
REFERENCES public.usuario (id_usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.userstory ADD CONSTRAINT usuario_userstory_fk
FOREIGN KEY (id_usuario)
REFERENCES public.usuario (id_usuario)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reuniones ADD CONSTRAINT equipo_reuniones_fk
FOREIGN KEY (id_equipo)
REFERENCES public.equipo (id_equipo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.proyecto ADD CONSTRAINT equipo_proyecto_fk
FOREIGN KEY (id_equipo)
REFERENCES public.equipo (id_equipo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.userstory ADD CONSTRAINT proyecto_userstory_fk
FOREIGN KEY (id_proyecto)
REFERENCES public.proyecto (id_proyecto)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
