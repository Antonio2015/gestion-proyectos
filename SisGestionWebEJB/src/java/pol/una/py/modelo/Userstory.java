/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pol.una.py.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "userstory", catalog = "proyecto", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Userstory.findAll", query = "SELECT u FROM Userstory u")
    , @NamedQuery(name = "Userstory.findByIdUserstory", query = "SELECT u FROM Userstory u WHERE u.idUserstory = :idUserstory")
    , @NamedQuery(name = "Userstory.findByNombreLargo", query = "SELECT u FROM Userstory u WHERE u.nombreLargo = :nombreLargo")
    , @NamedQuery(name = "Userstory.findByTiempoAsignado", query = "SELECT u FROM Userstory u WHERE u.tiempoAsignado = :tiempoAsignado")
    , @NamedQuery(name = "Userstory.findByDescripcion", query = "SELECT u FROM Userstory u WHERE u.descripcion = :descripcion")
    , @NamedQuery(name = "Userstory.findByNombreLargo1", query = "SELECT u FROM Userstory u WHERE u.nombreLargo1 = :nombreLargo1")
    , @NamedQuery(name = "Userstory.findByValorNegocio", query = "SELECT u FROM Userstory u WHERE u.valorNegocio = :valorNegocio")
    , @NamedQuery(name = "Userstory.findByEstado", query = "SELECT u FROM Userstory u WHERE u.estado = :estado")
    , @NamedQuery(name = "Userstory.findByFechaInicio", query = "SELECT u FROM Userstory u WHERE u.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "Userstory.findByFechaFin", query = "SELECT u FROM Userstory u WHERE u.fechaFin = :fechaFin")})
public class Userstory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_userstory", nullable = false)
    private Integer idUserstory;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre_largo", nullable = false, length = 2147483647)
    private String nombreLargo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "tiempo_asignado", nullable = false, length = 2147483647)
    private String tiempoAsignado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion", nullable = false, length = 2147483647)
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre_largo_1", nullable = false, length = 2147483647)
    private String nombreLargo1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "valor_negocio", nullable = false, length = 2147483647)
    private String valorNegocio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado", nullable = false)
    private int estado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "fecha_fin", nullable = false, length = 2147483647)
    private String fechaFin;
    @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto", nullable = false)
    @ManyToOne(optional = false)
    private Proyecto idProyecto;
    @JoinColumn(name = "id_sprint", referencedColumnName = "id_sprint", nullable = false)
    @ManyToOne(optional = false)
    private Sprint idSprint;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario", nullable = false)
    @ManyToOne(optional = false)
    private Usuario idUsuario;

    public Userstory() {
    }

    public Userstory(Integer idUserstory) {
        this.idUserstory = idUserstory;
    }

    public Userstory(Integer idUserstory, String nombreLargo, String tiempoAsignado, String descripcion, String nombreLargo1, String valorNegocio, int estado, Date fechaInicio, String fechaFin) {
        this.idUserstory = idUserstory;
        this.nombreLargo = nombreLargo;
        this.tiempoAsignado = tiempoAsignado;
        this.descripcion = descripcion;
        this.nombreLargo1 = nombreLargo1;
        this.valorNegocio = valorNegocio;
        this.estado = estado;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public Integer getIdUserstory() {
        return idUserstory;
    }

    public void setIdUserstory(Integer idUserstory) {
        this.idUserstory = idUserstory;
    }

    public String getNombreLargo() {
        return nombreLargo;
    }

    public void setNombreLargo(String nombreLargo) {
        this.nombreLargo = nombreLargo;
    }

    public String getTiempoAsignado() {
        return tiempoAsignado;
    }

    public void setTiempoAsignado(String tiempoAsignado) {
        this.tiempoAsignado = tiempoAsignado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreLargo1() {
        return nombreLargo1;
    }

    public void setNombreLargo1(String nombreLargo1) {
        this.nombreLargo1 = nombreLargo1;
    }

    public String getValorNegocio() {
        return valorNegocio;
    }

    public void setValorNegocio(String valorNegocio) {
        this.valorNegocio = valorNegocio;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Proyecto getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Proyecto idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Sprint getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(Sprint idSprint) {
        this.idSprint = idSprint;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUserstory != null ? idUserstory.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userstory)) {
            return false;
        }
        Userstory other = (Userstory) object;
        if ((this.idUserstory == null && other.idUserstory != null) || (this.idUserstory != null && !this.idUserstory.equals(other.idUserstory))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.Userstory[ idUserstory=" + idUserstory + " ]";
    }
    
}
