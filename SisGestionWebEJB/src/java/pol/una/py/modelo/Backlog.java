/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pol.una.py.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "backlog", catalog = "proyecto", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Backlog.findAll", query = "SELECT b FROM Backlog b")
    , @NamedQuery(name = "Backlog.findByIdBacklog", query = "SELECT b FROM Backlog b WHERE b.idBacklog = :idBacklog")
    , @NamedQuery(name = "Backlog.findByNombreBacklog", query = "SELECT b FROM Backlog b WHERE b.nombreBacklog = :nombreBacklog")
    , @NamedQuery(name = "Backlog.findByPrioridad", query = "SELECT b FROM Backlog b WHERE b.prioridad = :prioridad")
    , @NamedQuery(name = "Backlog.findBySprint", query = "SELECT b FROM Backlog b WHERE b.sprint = :sprint")})
public class Backlog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_backlog", nullable = false)
    private Integer idBacklog;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre_backlog", nullable = false, length = 2147483647)
    private String nombreBacklog;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prioridad", nullable = false)
    private int prioridad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sprint", nullable = false)
    private int sprint;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idBacklog")
    private List<Sprint> sprintList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idBacklog")
    private List<Proyecto> proyectoList;

    public Backlog() {
    }

    public Backlog(Integer idBacklog) {
        this.idBacklog = idBacklog;
    }

    public Backlog(Integer idBacklog, String nombreBacklog, int prioridad, int sprint) {
        this.idBacklog = idBacklog;
        this.nombreBacklog = nombreBacklog;
        this.prioridad = prioridad;
        this.sprint = sprint;
    }

    public Integer getIdBacklog() {
        return idBacklog;
    }

    public void setIdBacklog(Integer idBacklog) {
        this.idBacklog = idBacklog;
    }

    public String getNombreBacklog() {
        return nombreBacklog;
    }

    public void setNombreBacklog(String nombreBacklog) {
        this.nombreBacklog = nombreBacklog;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public int getSprint() {
        return sprint;
    }

    public void setSprint(int sprint) {
        this.sprint = sprint;
    }

    @XmlTransient
    public List<Sprint> getSprintList() {
        return sprintList;
    }

    public void setSprintList(List<Sprint> sprintList) {
        this.sprintList = sprintList;
    }

    @XmlTransient
    public List<Proyecto> getProyectoList() {
        return proyectoList;
    }

    public void setProyectoList(List<Proyecto> proyectoList) {
        this.proyectoList = proyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBacklog != null ? idBacklog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Backlog)) {
            return false;
        }
        Backlog other = (Backlog) object;
        if ((this.idBacklog == null && other.idBacklog != null) || (this.idBacklog != null && !this.idBacklog.equals(other.idBacklog))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.Backlog[ idBacklog=" + idBacklog + " ]";
    }
    
}
