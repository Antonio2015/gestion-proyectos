/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pol.una.py.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import pol.una.py.dao.UsuarioDao;
import pol.una.py.modelo.Usuario;

/**
 *
 * @author apaiva
 */
public class UsuarioDaoImpl implements UsuarioDao {

    private EntityTransaction tx;
    private EntityManagerFactory emf;
    private EntityManager em;

    public static void main(String[] args) {
        System.out.println(" iniciando  ...");
        UsuarioDaoImpl daoImpl = new UsuarioDaoImpl();
        daoImpl.getSession();
    }
    
    public UsuarioDaoImpl() {
        getSession();
    }

    public void getSession() {
        emf = Persistence.createEntityManagerFactory("SisGestionWebEJBPU");
        em = emf.createEntityManager();
        System.out.println(" em " + em.toString());
    }

    @Override
    public Usuario create(Usuario usuario) {
        try {
            em.getTransaction().begin();
            em.persist(usuario);
            em.getTransaction().commit();
            //logger.info("Se inserta el modulo con id:" + facturaDetalle.getIdFactura());
            return usuario;
        } catch (Exception e) {
            // logger.error("CLASS " + facturaDetalle.getIdFactura() + " METHOD: create ", e);
            System.out.println(" mensaje " + e.getMessage());
            return null;
        }

    }

    @Override
    public Usuario update(Usuario usuario) {
        try {
            em.getTransaction().begin();
            em.merge(usuario);
            em.getTransaction().commit();
            //logger.info("Se inserta el modulo con id:" + facturaDetalle.getIdFactura());
            return usuario;
        } catch (Exception e) {
            // logger.error("CLASS " + facturaDetalle.getIdFactura() + " METHOD: create ", e);
            return null;
        }
    }

    @Override
    public boolean delete(Usuario usuario) {

        try {
            Integer idFactura = usuario.getIdUsuario();
            em.remove(em.find(Usuario.class, usuario));
            em.flush();
            //logger.info("Se elimina el modulo con Id: " + nro_cuestionario);
            return true;
        } catch (Exception e) {
            //logger.error("CLASS " + this.getClass().getName() + " METHOD: eliminar ", e);
            return false;
        }

    }

    @Override
    public Usuario getById(Integer id) {
        //        
        try {
            Usuario usuario = em.find(Usuario.class, id);
            if (!(usuario == null)) {
                // logger.info(" id encontrado" + id + "  ");
            }
            return usuario;
        } catch (Exception e) {
            //   logger.error("CLASS " + this.getClass().getName() + " METHOD: obtenerPorId ", e);
            // logger.info(" id no encontrado" + id);
            return null;
        }
    }

    @Override
    public List<Usuario> getAll() {

        try {
            return em.createQuery("SELECT u FROM Usuario u").getResultList();
        } catch (Exception e) {
            //  logger.error("CLASS " + this.getClass().getName() + " METHOD: obtenerTodos ", e);
            return null;
        }
    }

}
