/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pol.una.py.dao;
import pol.una.py.modelo.Usuario;

/**
 *UsuarioDao, Integer
 * @author apaiva
 */
public interface UsuarioDao extends GenericDao<Usuario, Integer>{
    
}
