/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pol.una.py.dao;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author apaiva
 */
public interface GenericDao<T, K extends Serializable> {

    T create(T object);

    T update(T object);

    boolean delete(T object);

    T getById(K id);

    List<T> getAll();

}
