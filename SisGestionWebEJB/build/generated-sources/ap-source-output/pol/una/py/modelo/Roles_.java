package pol.una.py.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.py.modelo.Permiso;
import pol.una.py.modelo.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-08T02:13:37")
@StaticMetamodel(Roles.class)
public class Roles_ { 

    public static volatile SingularAttribute<Roles, Integer> idRol;
    public static volatile SingularAttribute<Roles, String> descripcionRol;
    public static volatile ListAttribute<Roles, Usuario> usuarioList;
    public static volatile SingularAttribute<Roles, String> nombreRol;
    public static volatile ListAttribute<Roles, Permiso> permisoList;

}