package pol.una.py.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.py.modelo.Proyecto;
import pol.una.py.modelo.Reuniones;
import pol.una.py.modelo.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-08T02:13:37")
@StaticMetamodel(Equipo.class)
public class Equipo_ { 

    public static volatile ListAttribute<Equipo, Proyecto> proyectoList;
    public static volatile SingularAttribute<Equipo, Integer> idEquipo;
    public static volatile SingularAttribute<Equipo, String> rolEquipo;
    public static volatile SingularAttribute<Equipo, String> nombreEquipo;
    public static volatile SingularAttribute<Equipo, Usuario> idUsuario;
    public static volatile ListAttribute<Equipo, Reuniones> reunionesList;

}