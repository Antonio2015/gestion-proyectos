package pol.una.py.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.py.modelo.Backlog;
import pol.una.py.modelo.Estado;
import pol.una.py.modelo.Userstory;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-08T02:13:37")
@StaticMetamodel(Sprint.class)
public class Sprint_ { 

    public static volatile SingularAttribute<Sprint, Estado> idEstado;
    public static volatile SingularAttribute<Sprint, Date> fechaInicio;
    public static volatile ListAttribute<Sprint, Userstory> userstoryList;
    public static volatile SingularAttribute<Sprint, Integer> idSprint;
    public static volatile SingularAttribute<Sprint, String> duracion;
    public static volatile SingularAttribute<Sprint, String> nombre;
    public static volatile SingularAttribute<Sprint, Date> fechaFin;
    public static volatile SingularAttribute<Sprint, String> observacion;
    public static volatile SingularAttribute<Sprint, Backlog> idBacklog;

}