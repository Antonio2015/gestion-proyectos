package pol.una.py.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.py.modelo.Equipo;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-08T02:13:37")
@StaticMetamodel(Reuniones.class)
public class Reuniones_ { 

    public static volatile SingularAttribute<Reuniones, Integer> idReunion;
    public static volatile SingularAttribute<Reuniones, Equipo> idEquipo;
    public static volatile SingularAttribute<Reuniones, Date> fechaReunion;
    public static volatile SingularAttribute<Reuniones, String> observacion;

}