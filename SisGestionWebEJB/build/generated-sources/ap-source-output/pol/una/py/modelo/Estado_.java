package pol.una.py.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.py.modelo.Proyecto;
import pol.una.py.modelo.Sprint;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-08T02:13:37")
@StaticMetamodel(Estado.class)
public class Estado_ { 

    public static volatile ListAttribute<Estado, Proyecto> proyectoList;
    public static volatile SingularAttribute<Estado, Integer> idEstado;
    public static volatile SingularAttribute<Estado, String> nombreEstado;
    public static volatile ListAttribute<Estado, Sprint> sprintList;

}