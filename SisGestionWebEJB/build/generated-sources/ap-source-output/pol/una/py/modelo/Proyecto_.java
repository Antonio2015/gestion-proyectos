package pol.una.py.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.py.modelo.Backlog;
import pol.una.py.modelo.Equipo;
import pol.una.py.modelo.Estado;
import pol.una.py.modelo.Userstory;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-08T02:13:37")
@StaticMetamodel(Proyecto.class)
public class Proyecto_ { 

    public static volatile SingularAttribute<Proyecto, Integer> idProyecto;
    public static volatile SingularAttribute<Proyecto, String> cantidadSprint;
    public static volatile SingularAttribute<Proyecto, Equipo> idEquipo;
    public static volatile SingularAttribute<Proyecto, Estado> idEstado;
    public static volatile SingularAttribute<Proyecto, String> anhoProyecto;
    public static volatile ListAttribute<Proyecto, Userstory> userstoryList;
    public static volatile SingularAttribute<Proyecto, String> nombreProyecto;
    public static volatile SingularAttribute<Proyecto, Date> fechaFinEstimada;
    public static volatile SingularAttribute<Proyecto, Date> fechaFin;
    public static volatile SingularAttribute<Proyecto, String> estadoProyecto;
    public static volatile SingularAttribute<Proyecto, Backlog> idBacklog;

}