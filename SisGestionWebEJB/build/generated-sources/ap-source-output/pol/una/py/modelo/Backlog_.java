package pol.una.py.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.py.modelo.Proyecto;
import pol.una.py.modelo.Sprint;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-08T02:13:37")
@StaticMetamodel(Backlog.class)
public class Backlog_ { 

    public static volatile ListAttribute<Backlog, Proyecto> proyectoList;
    public static volatile SingularAttribute<Backlog, String> nombreBacklog;
    public static volatile ListAttribute<Backlog, Sprint> sprintList;
    public static volatile SingularAttribute<Backlog, Integer> sprint;
    public static volatile SingularAttribute<Backlog, Integer> prioridad;
    public static volatile SingularAttribute<Backlog, Integer> idBacklog;

}