package pol.una.py.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.py.modelo.Proyecto;
import pol.una.py.modelo.Sprint;
import pol.una.py.modelo.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-08T02:13:37")
@StaticMetamodel(Userstory.class)
public class Userstory_ { 

    public static volatile SingularAttribute<Userstory, String> descripcion;
    public static volatile SingularAttribute<Userstory, String> valorNegocio;
    public static volatile SingularAttribute<Userstory, Proyecto> idProyecto;
    public static volatile SingularAttribute<Userstory, Integer> estado;
    public static volatile SingularAttribute<Userstory, String> nombreLargo;
    public static volatile SingularAttribute<Userstory, Date> fechaInicio;
    public static volatile SingularAttribute<Userstory, Usuario> idUsuario;
    public static volatile SingularAttribute<Userstory, Integer> idUserstory;
    public static volatile SingularAttribute<Userstory, Sprint> idSprint;
    public static volatile SingularAttribute<Userstory, String> nombreLargo1;
    public static volatile SingularAttribute<Userstory, String> fechaFin;
    public static volatile SingularAttribute<Userstory, String> tiempoAsignado;

}