package pol.una.py.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pol.una.py.modelo.Roles;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-08T02:13:37")
@StaticMetamodel(Permiso.class)
public class Permiso_ { 

    public static volatile SingularAttribute<Permiso, Roles> idRol;
    public static volatile SingularAttribute<Permiso, Integer> idPermiso;
    public static volatile SingularAttribute<Permiso, String> nombrePermiso;

}