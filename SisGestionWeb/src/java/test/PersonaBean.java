/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author apaiva
 */
@Named(value = "personaBean")
@SessionScoped
public class PersonaBean implements Serializable {

    private String nombre;
    private String saludar;
    
    public PersonaBean() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSaludar() {
        return saludar;
    }

    public void setSaludar(String saludar) {
        this.saludar = saludar;
    }
    
    public void saludarJSF(){
        this.saludar= "probaado " + nombre;
    }
    
    //"home.xhtml?faces-redirect=true";
    public String loginControl() {
        return "home";
    }
}
