/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author USER
 */
@Named(value = "menuController")
@SessionScoped
public class menuController implements Serializable {

    //lista de menus 
    private List<String> listMenus;
    private MenuModel model;

    public menuController() {
    }

    @PostConstruct
    public void init() {
        //this.listarMenus();
        model = new DefaultMenuModel();
        this.establecerPermisos();
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public void listarMenus() {
        listMenus.add("OPCION1");
        listMenus.add("OPCION2");
//        listMenus.add("OPCION3");
//        listMenus.add("OPCION4");
//        listMenus.add("OPCION5");

    }

    public void establecerPermisos() {

        // definir los menus 
        // for (String m : listMenus) {
        DefaultSubMenu subMenu = new DefaultSubMenu("Opcion");
        DefaultMenuItem item = new DefaultMenuItem("Item");
        subMenu.addElement(item);

//            for (String listMenu : listMenus) {
//                DefaultMenuItem item = new DefaultMenuItem("Item");
//                subMenu.addElement(item);
//
//            }
        model.addElement(subMenu);
        //  }
    }

}
