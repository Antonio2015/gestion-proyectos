/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.management.Query;
import org.primefaces.context.RequestContext;

/**
 *
 * @author apaiva
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private String username;
    private String password;
    private final Query query = new Query();

    public String loginControl() {

        if (username.trim().equals(password.trim())) {
            System.out.println("ingresado al sistema ");
            return "template";
        }

        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Error Iniciar Sesión",
                "Usuario o Contraseña Incorrectos: "));
        
        
//        context.addMessage(null, new FacesMessage("Second Message", "Additional Message Detail"));

        return "";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
