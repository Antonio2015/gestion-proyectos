-- Drop table

-- DROP TABLE public.backlog;

CREATE TABLE public.backlog (
	id_backlog serial NOT NULL,
	nombre_backlog varchar NOT NULL,
	descripcion varchar(500) NULL,
	id_proyecto int4 NOT NULL,
	inactivo bool NULL,
	CONSTRAINT id_backlog_pk PRIMARY KEY (id_backlog),
	CONSTRAINT backlog_fk FOREIGN KEY (id_proyecto) REFERENCES proyecto(id_proyecto)
);

-- Drop table

-- DROP TABLE public.equipo;

CREATE TABLE public.equipo (
	id_equipo serial NOT NULL,
	id_usuario int4 NOT NULL,
	nombre_equipo varchar NOT NULL,
	rol_equipo varchar NOT NULL,
	CONSTRAINT id_equipo_pk PRIMARY KEY (id_equipo),
	CONSTRAINT usuario_equipo_fk FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario)
);

-- Drop table

-- DROP TABLE public.equipo_det;

CREATE TABLE public.equipo_det (
	id serial NOT NULL,
	id_equipo int4 NOT NULL,
	id_usuario int4 NOT NULL,
	CONSTRAINT equipo_det_pk PRIMARY KEY (id),
	CONSTRAINT equipo_det_fk FOREIGN KEY (id_equipo) REFERENCES equipo(id_equipo),
	CONSTRAINT equipo_det_fk_1 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario)
);

-- Drop table

-- DROP TABLE public.estado;

CREATE TABLE public.estado (
	id_estado serial NOT NULL,
	nombre_estado varchar NOT NULL,
	codigo varchar(5) NOT NULL,
	CONSTRAINT id_estado PRIMARY KEY (id_estado)
);

-- Drop table

-- DROP TABLE public.menu;

CREATE TABLE public.menu (
	id_menu serial NOT NULL,
	nombre varchar(100) NULL,
	CONSTRAINT pk_id_menu PRIMARY KEY (id_menu)
);

-- Drop table

-- DROP TABLE public.menu_submenu;

CREATE TABLE public.menu_submenu (
	id_menu int4 NULL,
	id_submenu int4 NULL,
	id serial NOT NULL,
	CONSTRAINT menu_submenu_pk PRIMARY KEY (id),
	CONSTRAINT menu_submenu_un UNIQUE (id_submenu, id_menu),
	CONSTRAINT menu_submenu_fk FOREIGN KEY (id_menu) REFERENCES menu(id_menu),
	CONSTRAINT menu_submenu_fk_1 FOREIGN KEY (id_submenu) REFERENCES submenu(id_submenu)
);

-- Drop table

-- DROP TABLE public.pantallas;

CREATE TABLE public.pantallas (
	id_pantalla serial NOT NULL,
	nombre varchar(100) NULL,
	url varchar(100) NULL,
	CONSTRAINT pk_id_pantalla PRIMARY KEY (id_pantalla)
);

-- Drop table

-- DROP TABLE public.permiso;

CREATE TABLE public.permiso (
	id_permiso serial NOT NULL,
	id_rol int4 NOT NULL,
	nombre_permiso varchar NOT NULL,
	CONSTRAINT id_permiso_pk PRIMARY KEY (id_permiso),
	CONSTRAINT roles_permiso_fk FOREIGN KEY (id_rol) REFERENCES roles(id_rol)
);

-- Drop table

-- DROP TABLE public.proyecto;

CREATE TABLE public.proyecto (
	id_proyecto serial NOT NULL,
	id_estado int4 NOT NULL,
	id_equipo int4 NOT NULL,
	nombre_proyecto varchar NOT NULL,
	fecha_fin date NULL,
	cantidad_sprint varchar NOT NULL,
	fecha_fin_estimada date NOT NULL,
	anho_proyecto varchar NOT NULL,
	codigo varchar(30) NULL,
	fecha_inicio date NULL,
	inactivo bool NULL,
	CONSTRAINT id_proyecto_pk PRIMARY KEY (id_proyecto),
	CONSTRAINT equipo_proyecto_fk FOREIGN KEY (id_equipo) REFERENCES equipo(id_equipo),
	CONSTRAINT estado_proyecto_fk FOREIGN KEY (id_estado) REFERENCES estado(id_estado)
);

-- Drop table

-- DROP TABLE public.proyecto_clientes;

CREATE TABLE public.proyecto_clientes (
	id_proyecto_cliente serial NOT NULL,
	id_proyecto int4 NULL,
	id_usuario int4 NULL,
	CONSTRAINT pk_id_proyecto_cliente PRIMARY KEY (id_proyecto_cliente),
	CONSTRAINT fk_id_proyecto FOREIGN KEY (id_proyecto) REFERENCES proyecto(id_proyecto),
	CONSTRAINT fk_id_usuario FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario)
);

-- Drop table

-- DROP TABLE public.reuniones;

CREATE TABLE public.reuniones (
	id_reunion serial NOT NULL,
	id_equipo int4 NOT NULL,
	fecha_reunion date NOT NULL,
	observacion varchar NOT NULL,
	CONSTRAINT id_reuniones_pk PRIMARY KEY (id_reunion),
	CONSTRAINT equipo_reuniones_fk FOREIGN KEY (id_equipo) REFERENCES equipo(id_equipo)
);

-- Drop table

-- DROP TABLE public.roles;

CREATE TABLE public.roles (
	id_rol serial NOT NULL,
	nombre_rol varchar NOT NULL,
	descripcion_rol varchar NOT NULL,
	CONSTRAINT id_rol_pk PRIMARY KEY (id_rol)
);

-- Drop table

-- DROP TABLE public.rolespantalla;

CREATE TABLE public.rolespantalla (
	id_roles_pantalla serial NOT NULL,
	id_pantalla int4 NULL,
	id_rol int4 NULL,
	CONSTRAINT pk_id_roles_pantalla PRIMARY KEY (id_roles_pantalla),
	CONSTRAINT fk_id_pantalla FOREIGN KEY (id_pantalla) REFERENCES pantallas(id_pantalla),
	CONSTRAINT fk_id_rol FOREIGN KEY (id_rol) REFERENCES roles(id_rol)
);

-- Drop table

-- DROP TABLE public.sprint;

CREATE TABLE public.sprint (
	id_sprint serial NOT NULL,
	id_estado int4 NOT NULL,
	nombre varchar NOT NULL,
	duracion varchar NOT NULL,
	fecha_inicio date NOT NULL,
	fecha_fin date NOT NULL,
	observacion varchar NOT NULL,
	id_backlog int4 NOT NULL,
	inactivo bool NULL,
	CONSTRAINT id_sprint_pk PRIMARY KEY (id_sprint),
	CONSTRAINT estado_sprint_fk FOREIGN KEY (id_estado) REFERENCES estado(id_estado),
	CONSTRAINT sprint_fk FOREIGN KEY (id_backlog) REFERENCES backlog(id_backlog)
);

-- Drop table

-- DROP TABLE public.sprint_det;

CREATE TABLE public.sprint_det (
	id_sprint_det serial NOT NULL,
	id_userstory int4 NULL,
	id_sprint int4 NULL,
	CONSTRAINT sprint_det_pk PRIMARY KEY (id_sprint_det),
	CONSTRAINT id_sprint_det_fk FOREIGN KEY (id_sprint) REFERENCES sprint(id_sprint),
	CONSTRAINT sprint_det_fk FOREIGN KEY (id_userstory) REFERENCES userstory(id_userstory)
);

-- Drop table

-- DROP TABLE public.submenu;

CREATE TABLE public.submenu (
	id_submenu serial NOT NULL,
	id_pantalla int4 NULL,
	nombre varchar(100) NULL,
	icono varchar(30) NULL,
	CONSTRAINT pf_id_submenu PRIMARY KEY (id_submenu),
	CONSTRAINT fd_id_pantalla FOREIGN KEY (id_pantalla) REFERENCES pantallas(id_pantalla)
);

-- Drop table

-- DROP TABLE public.userstory;

CREATE TABLE public.userstory (
	id_userstory serial NOT NULL,
	id_usuario int4 NOT NULL,
	nombre_largo varchar NOT NULL,
	tiempo_asignado varchar NOT NULL,
	descripcion varchar NOT NULL,
	nombre_corto varchar NOT NULL,
	valor_negocio varchar NULL,
	fecha_inicio date NOT NULL,
	valor_tecnico varchar NULL,
	fecha_fin date NOT NULL,
	id_backlog int4 NULL,
	id_estado int4 NULL,
	CONSTRAINT id_userstory PRIMARY KEY (id_userstory),
	CONSTRAINT userstory2_fk FOREIGN KEY (id_estado) REFERENCES estado(id_estado),
	CONSTRAINT userstory_fk FOREIGN KEY (id_backlog) REFERENCES backlog(id_backlog),
	CONSTRAINT usuario_userstory_fk FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario)
);

-- Drop table

-- DROP TABLE public.usuario;

CREATE TABLE public.usuario (
	id_usuario serial NOT NULL,
	id_rol int4 NOT NULL,
	nombre_usuario varchar NOT NULL,
	"password" varchar NOT NULL,
	cedula varchar NOT NULL,
	nombre varchar NULL,
	apellido varchar NULL,
	direccionn varchar NOT NULL,
	telefono varchar NOT NULL,
	email varchar NOT NULL,
	es_cliente bool NOT NULL,
	nombre_empresa varchar NULL,
	CONSTRAINT id_usuario_pk PRIMARY KEY (id_usuario),
	CONSTRAINT roles_usuario_fk FOREIGN KEY (id_rol) REFERENCES roles(id_rol)
);
