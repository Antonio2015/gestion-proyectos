/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.facultad.is2.model.Sprint;
import py.com.facultad.is2.model.SprintDet;
import py.com.facultad.is2.model.Userstory;

/**
 *
 * @author victoro
 */
@Stateless
public class SprintDetFacade extends AbstractFacade<SprintDet> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SprintDetFacade() {
        super(SprintDet.class);
    }

    public List<SprintDet> obtenerListaSprintDetallePorSprint(Sprint sprint) {
        List<SprintDet> sprintDetList = new ArrayList<>();
        try {
            Query q = em.createQuery("Select s from SprintDet s where s.idSprint = :sprint")
                    .setParameter("sprint", sprint);

            sprintDetList = q.getResultList();
        } catch (Exception e) {

        }

        return sprintDetList;

    }

    public SprintDet obtenerSprintDet(Sprint sprint, Userstory u) {
        SprintDet m = null;
        try {
            Query q = em.createQuery("Select s from SprintDet s where s.idSprint   = :sprint and s.idUserstory = :us", SprintDet.class)
                    .setParameter("sprint", sprint)
                    .setParameter("us", u);
            m = (SprintDet) q.getSingleResult();
        } catch (Exception e) {

        }

        return m;
    }

    public List<SprintDet> cargarTareas(Sprint sprintSeleccionado) {
        List<SprintDet> list = new ArrayList<>();

        try {
            Query q = em.createQuery("Select s from SprintDet s where s.idSprint = :sprint")
                    .setParameter("sprint", sprintSeleccionado);

            list = q.getResultList();
        } catch (Exception e) {

        }
        return list;
    }

    public List<SprintDet> cargarTareasPendientes(Sprint sprintSeleccionado) {
        List<SprintDet> list = new ArrayList<>();

        try {
            Query q = em.createQuery("Select s from SprintDet s where s.idSprint = :sprint and s.idUserstory.idEstado.codigo = 'PEN'")
                    .setParameter("sprint", sprintSeleccionado);

            list = q.getResultList();
        } catch (Exception e) {

        }
        return list;
    }

    public List<SprintDet> cargarTareasEnCurso(Sprint sprintSeleccionado) {
        List<SprintDet> list = new ArrayList<>();

        try {
            Query q = em.createQuery("Select s from SprintDet s where s.idSprint = :sprint and s.idUserstory.idEstado.codigo = 'CUR'")
                    .setParameter("sprint", sprintSeleccionado);

            list = q.getResultList();
        } catch (Exception e) {

        }
        return list;

    }

    public List<SprintDet> cargarTareasTerminadas(Sprint sprintSeleccionado) {
        List<SprintDet> list = new ArrayList<>();

        try {
            Query q = em.createQuery("Select s from SprintDet s where s.idSprint = :sprint and s.idUserstory.idEstado.codigo = 'TER'")
                    .setParameter("sprint", sprintSeleccionado);

            list = q.getResultList();
        } catch (Exception e) {

        }
        return list;

    }

}
