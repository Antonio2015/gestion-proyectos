/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author victoro
 */
@Entity
@Table(name = "menu_submenu")
@NamedQueries({
    @NamedQuery(name = "MenuSubmenu.findAll", query = "SELECT m FROM MenuSubmenu m")
    , @NamedQuery(name = "MenuSubmenu.findById", query = "SELECT m FROM MenuSubmenu m WHERE m.id = :id")})
public class MenuSubmenu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @JoinColumn(name = "id_menu", referencedColumnName = "id_menu")
    @ManyToOne
    private Menu idMenu;
    @JoinColumn(name = "id_submenu", referencedColumnName = "id_submenu")
    @ManyToOne
    private Submenu idSubmenu;

    public MenuSubmenu() {
    }

    public MenuSubmenu(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Menu getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(Menu idMenu) {
        this.idMenu = idMenu;
    }

    public Submenu getIdSubmenu() {
        return idSubmenu;
    }

    public void setIdSubmenu(Submenu idSubmenu) {
        this.idSubmenu = idSubmenu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuSubmenu)) {
            return false;
        }
        MenuSubmenu other = (MenuSubmenu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.facultad.is2.model.MenuSubmenu[ id=" + id + " ]";
    }
    
}
