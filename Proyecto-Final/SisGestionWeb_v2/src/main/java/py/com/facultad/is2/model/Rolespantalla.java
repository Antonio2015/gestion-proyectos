/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author victoro
 */
@Entity
@Table(name = "rolespantalla")
@NamedQueries({
    @NamedQuery(name = "Rolespantalla.findAll", query = "SELECT r FROM Rolespantalla r")
    , @NamedQuery(name = "Rolespantalla.findByIdRolesPantalla", query = "SELECT r FROM Rolespantalla r WHERE r.idRolesPantalla = :idRolesPantalla")})
public class Rolespantalla implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_roles_pantalla", nullable = false)
    private Integer idRolesPantalla;
    @JoinColumn(name = "id_pantalla", referencedColumnName = "id_pantalla")
    @ManyToOne
    private Pantallas idPantalla;
    @JoinColumn(name = "id_rol", referencedColumnName = "id_rol")
    @ManyToOne
    private Roles idRol;

    public Rolespantalla() {
    }

    public Rolespantalla(Integer idRolesPantalla) {
        this.idRolesPantalla = idRolesPantalla;
    }

    public Integer getIdRolesPantalla() {
        return idRolesPantalla;
    }

    public void setIdRolesPantalla(Integer idRolesPantalla) {
        this.idRolesPantalla = idRolesPantalla;
    }

    public Pantallas getIdPantalla() {
        return idPantalla;
    }

    public void setIdPantalla(Pantallas idPantalla) {
        this.idPantalla = idPantalla;
    }

    public Roles getIdRol() {
        return idRol;
    }

    public void setIdRol(Roles idRol) {
        this.idRol = idRol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRolesPantalla != null ? idRolesPantalla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rolespantalla)) {
            return false;
        }
        Rolespantalla other = (Rolespantalla) object;
        if ((this.idRolesPantalla == null && other.idRolesPantalla != null) || (this.idRolesPantalla != null && !this.idRolesPantalla.equals(other.idRolesPantalla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.facultad.is2.model.Rolespantalla[ idRolesPantalla=" + idRolesPantalla + " ]";
    }
    
}
