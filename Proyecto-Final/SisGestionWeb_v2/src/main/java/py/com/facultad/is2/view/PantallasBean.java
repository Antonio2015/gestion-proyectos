/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;
import py.com.facultad.is2.facade.PantallasFacade;
import py.com.facultad.is2.facade.RolesFacade;
import py.com.facultad.is2.model.Pantallas;
import py.com.facultad.is2.model.Roles;

/**
 *
 * @author victoro
 */
@ManagedBean
@SessionScoped
public class PantallasBean extends AbstractBean implements Serializable {

    private List<Pantallas> listaPantallas = new ArrayList<>();
    private Pantallas pantallaSeleccionada;
    private boolean editando;
    private DualListModel<Roles> rolesDualList;

    @EJB
    private PantallasFacade pantallasEJB;
    @EJB
    private RolesFacade rolesEJB;

    @PostConstruct
    public void init() {
        pantallaSeleccionada = new Pantallas();
        listaPantallas = pantallasEJB.findAll();
        rolesDualList = new DualListModel<Roles>(new ArrayList<Roles>(), new ArrayList<Roles>());
    }

    @Override
    public void resetearValores() {
        pantallaSeleccionada = new Pantallas();
        editando = false;
    }

    @Override
    public void inicializarListas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void guardar() {
        try {
            pantallasEJB.create(pantallaSeleccionada, rolesDualList.getTarget());
            infoMessage("Se guardó correctamente.");
            listaPantallas = pantallasEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbPantallas').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void antesActualizar() {
        editando = true;
        cargarListaRoles();
    }

    public void cargarListaRoles() {
        List<Roles> source = rolesEJB.rolesSource(pantallaSeleccionada);
        List<Roles> target = rolesEJB.rolesTarget(pantallaSeleccionada);
        rolesDualList = new DualListModel<Roles>(source, target);
    }

    @Override
    public void actualizar() {
        try {
            pantallasEJB.edit(pantallaSeleccionada, rolesDualList.getTarget());
            infoMessage("Se actualizó correctamente.");
            listaPantallas = pantallasEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbPantallas').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void eliminar() {
        try {
            pantallasEJB.remove(pantallaSeleccionada);
            infoMessage("Eliminado correctamente");
            listaPantallas = pantallasEJB.findAll();
        } catch (Exception e) {
            errorMessage("No se pudo eliminar el registro");
        }
    }

    public void agregarItem() {
        resetearValores();
        List<Roles> source = rolesEJB.findAll();
        List<Roles> target = new ArrayList<>();

        rolesDualList = new DualListModel<Roles>(source, target);
    }

    public List<Pantallas> getListaPantallas() {
        return listaPantallas;
    }

    public void setListaPantallas(List<Pantallas> listaPantallas) {
        this.listaPantallas = listaPantallas;
    }

    public Pantallas getPantallaSeleccionada() {
        return pantallaSeleccionada;
    }

    public void setPantallaSeleccionada(Pantallas pantallaSeleccionada) {
        this.pantallaSeleccionada = pantallaSeleccionada;
    }

    public boolean isEditando() {
        return editando;
    }

    public void setEditando(boolean editando) {
        this.editando = editando;
    }

    public DualListModel<Roles> getRolesDualList() {
        return rolesDualList;
    }

    public void setRolesDualList(DualListModel<Roles> rolesDualList) {
        this.rolesDualList = rolesDualList;
    }

}
