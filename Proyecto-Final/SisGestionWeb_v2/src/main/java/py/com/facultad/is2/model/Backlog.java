/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author victoro
 */
@Entity
@Table(name = "backlog")
@NamedQueries({
    @NamedQuery(name = "Backlog.findAll", query = "SELECT b FROM Backlog b")
    , @NamedQuery(name = "Backlog.findByIdBacklog", query = "SELECT b FROM Backlog b WHERE b.idBacklog = :idBacklog")
    , @NamedQuery(name = "Backlog.findByNombreBacklog", query = "SELECT b FROM Backlog b WHERE b.nombreBacklog = :nombreBacklog")})
public class Backlog implements Serializable {

    @Column(name = "inactivo")
    private Boolean inactivo;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idBacklog")
    private List<Sprint> sprintList;

    @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto", nullable = false)
    @ManyToOne(optional = false)
    private Proyecto idProyecto;
    @OneToMany(mappedBy = "idBacklog")
    private List<Userstory> userstoryList;

    @Size(max = 500)
    @Column(name = "descripcion", length = 500)
    private String descripcion;    

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_backlog", nullable = false)
    private Integer idBacklog;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre_backlog", nullable = false, length = 2147483647)
    private String nombreBacklog;   

    public Backlog() {
    }

    public Backlog(Integer idBacklog) {
        this.idBacklog = idBacklog;
    }

    public Backlog(Integer idBacklog, String nombreBacklog) {
        this.idBacklog = idBacklog;
        this.nombreBacklog = nombreBacklog;        
    }

    public Integer getIdBacklog() {
        return idBacklog;
    }

    public void setIdBacklog(Integer idBacklog) {
        this.idBacklog = idBacklog;
    }

    public String getNombreBacklog() {
        return nombreBacklog;
    }

    public void setNombreBacklog(String nombreBacklog) {
        this.nombreBacklog = nombreBacklog;
    }  


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBacklog != null ? idBacklog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Backlog)) {
            return false;
        }
        Backlog other = (Backlog) object;
        if ((this.idBacklog == null && other.idBacklog != null) || (this.idBacklog != null && !this.idBacklog.equals(other.idBacklog))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.facultad.is2.model.Backlog[ idBacklog=" + idBacklog + " ]";
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Proyecto getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Proyecto idProyecto) {
        this.idProyecto = idProyecto;
    }

    public List<Userstory> getUserstoryList() {
        return userstoryList;
    }

    public void setUserstoryList(List<Userstory> userstoryList) {
        this.userstoryList = userstoryList;
    }

    public List<Sprint> getSprintList() {
        return sprintList;
    }

    public void setSprintList(List<Sprint> sprintList) {
        this.sprintList = sprintList;
    }

    public Boolean getInactivo() {
        return inactivo;
    }

    public void setInactivo(Boolean inactivo) {
        this.inactivo = inactivo;
    }

   
    
}
