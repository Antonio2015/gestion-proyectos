/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.com.facultad.is2.model.Pantallas;
import py.com.facultad.is2.model.Roles;
import py.com.facultad.is2.model.Rolespantalla;

/**
 *
 * @author victoro
 */
@Stateless
public class PantallasFacade extends AbstractFacade<Pantallas> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;
    
    @EJB
    private RolespantallaFacade rolesPantallaEJB;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void create(Pantallas pantalla, List<Roles> rolesList){
        getEntityManager().persist(pantalla);
        List<Rolespantalla> listaRolesPantalla = rolesPantallaEJB.obtenerListaRolesPantalla(pantalla);
        List<Roles> rolesListAux = new ArrayList<>();
        List<Roles> aAgregar = new ArrayList<>();
        List<Roles> aEliminar = new ArrayList<>();
        for (Rolespantalla m : listaRolesPantalla) {
            if (!rolesListAux.contains(m.getIdRol())) {
                rolesListAux.add(m.getIdRol());
            }
        }

        for (Roles r : rolesListAux) {
            if (!rolesList.contains(r)) {
                aEliminar.add(r);
            }
        }

        for (Roles r : rolesList) {
            if (!rolesListAux.contains(r)) {
                aAgregar.add(r);
            }
        }

        for (Roles r : aEliminar) {
            Rolespantalla eliminar = rolesPantallaEJB.obtenerRolesPantalla(pantalla, r);
            if (eliminar != null) {
                rolesPantallaEJB.remove(eliminar);
            }

        }

        for (Roles r : aAgregar) {
            Rolespantalla  rp = new Rolespantalla();
            rp.setIdPantalla(pantalla);
            rp.setIdRol(r);
            rolesPantallaEJB.create(rp);
        }
    }
    
    public void edit(Pantallas pantalla, List<Roles> rolesList){
         getEntityManager().merge(pantalla);
        List<Rolespantalla> listaRolesPantalla = rolesPantallaEJB.obtenerListaRolesPantalla(pantalla);
        List<Roles> rolesListAux = new ArrayList<>();
        List<Roles> aAgregar = new ArrayList<>();
        List<Roles> aEliminar = new ArrayList<>();
        for (Rolespantalla m : listaRolesPantalla) {
            if (!rolesListAux.contains(m.getIdRol())) {
                rolesListAux.add(m.getIdRol());
            }
        }

        for (Roles r : rolesListAux) {
            if (!rolesList.contains(r)) {
                aEliminar.add(r);
            }
        }

        for (Roles r : rolesList) {
            if (!rolesListAux.contains(r)) {
                aAgregar.add(r);
            }
        }

        for (Roles r : aEliminar) {
            Rolespantalla eliminar = rolesPantallaEJB.obtenerRolesPantalla(pantalla, r);
            if (eliminar != null) {
                rolesPantallaEJB.remove(eliminar);
            }

        }

        for (Roles r : aAgregar) {
            Rolespantalla  rp = new Rolespantalla();
            rp.setIdPantalla(pantalla);
            rp.setIdRol(r);
            rolesPantallaEJB.create(rp);
        }
    }
    
    public PantallasFacade() {
        super(Pantallas.class);
    }
    
  
    
}
