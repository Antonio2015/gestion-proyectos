/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;
import py.com.facultad.is2.facade.BacklogFacade;
import py.com.facultad.is2.facade.ProyectoFacade;
import py.com.facultad.is2.facade.UserstoryFacade;
import py.com.facultad.is2.model.Backlog;
import py.com.facultad.is2.model.Proyecto;
import py.com.facultad.is2.model.Userstory;

/**
 *
 * @author victoro
 */
@ManagedBean
@SessionScoped
public class ProductBackLogBean extends AbstractBean implements Serializable {

    private List<Userstory> userStoryList = new ArrayList<>();
    private List<Backlog> backlogList = new ArrayList<>();
    private List<Proyecto> proyectosList = new ArrayList<>();

    private Userstory userStorySeleccionado;
    private Backlog backlogSeleccionado;
    private Proyecto proyectoSeleccionado;

    private boolean editando;

    @EJB
    UserstoryFacade userStoryEJB;

    @EJB
    BacklogFacade backlogEJB;

    @EJB
    ProyectoFacade proyectoEJB;

    @Override
    public void init() {
        backlogSeleccionado = new Backlog();
        proyectosList = proyectoEJB.findAll();
        backlogList = backlogEJB.findAll();
    }

    @Override
    public void resetearValores() {
        backlogSeleccionado = new Backlog();
        proyectoSeleccionado = null;
        editando = false;

    }

    public void agregarBL() {
        resetearValores();
        proyectosList = proyectoEJB.findAll();
    }

    @Override
    public void inicializarListas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//    @Override
    public void guardar() {
        try {
            backlogSeleccionado.setIdProyecto(proyectoSeleccionado);
            backlogEJB.create(backlogSeleccionado);
            infoMessage("Se guardó correctamente.");
            backlogList = backlogEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbProductBL').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void actualizar() {
        try {
            backlogSeleccionado.setIdProyecto(proyectoSeleccionado);
            backlogEJB.edit(backlogSeleccionado);
            infoMessage("Se actualizó correctamente.");
            backlogList = backlogEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbProductBL').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void eliminar() {
        try {
            backlogSeleccionado.setInactivo(true);
            backlogEJB.edit(backlogSeleccionado);
            infoMessage("Eliminado correctamente");
            backlogList = backlogEJB.findAll();
        } catch (Exception e) {
            errorMessage("No se pudo eliminar el registro");
        }
    }

    @Override
    public void antesActualizar() {
        proyectoSeleccionado = backlogSeleccionado.getIdProyecto();
        editando = true;
        proyectosList = proyectoEJB.findAll();
    }

    public List<Userstory> getUserStoryList() {
        return userStoryList;
    }

    public void setUserStoryList(List<Userstory> userStoryList) {
        this.userStoryList = userStoryList;
    }

    public Userstory getUserStorySeleccionado() {
        return userStorySeleccionado;
    }

    public void setUserStorySeleccionado(Userstory userStorySeleccionado) {
        this.userStorySeleccionado = userStorySeleccionado;
    }

    public boolean isEditando() {
        return editando;
    }

    public void setEditando(boolean editando) {
        this.editando = editando;
    }

    public List<Backlog> getBacklogList() {
        return backlogList;
    }

    public void setBacklogList(List<Backlog> backlogList) {
        this.backlogList = backlogList;
    }

    public Backlog getBacklogSeleccionado() {
        return backlogSeleccionado;
    }

    public void setBacklogSeleccionado(Backlog backlogSeleccionado) {
        this.backlogSeleccionado = backlogSeleccionado;
    }

    public List<Proyecto> getProyectosList() {
        return proyectosList;
    }

    public void setProyectosList(List<Proyecto> proyectosList) {
        this.proyectosList = proyectosList;
    }

    public Proyecto getProyectoSeleccionado() {
        return proyectoSeleccionado;
    }

    public void setProyectoSeleccionado(Proyecto proyectoSeleccionado) {
        this.proyectoSeleccionado = proyectoSeleccionado;
    }

    public void cargarUS() {
        userStoryList = userStoryEJB.findByBacklog(backlogSeleccionado);
    }

}
