/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.facultad.is2.model.Menu;
import py.com.facultad.is2.model.MenuSubmenu;
import py.com.facultad.is2.model.Submenu;

/**
 *
 * @author victoro
 */
@Stateless
public class MenuFacade extends AbstractFacade<Menu> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @EJB
    private SubmenuFacade submenuEjb;

    @EJB
    private MenuSubmenuFacade menuSubmenuEJB;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MenuFacade() {
        super(Menu.class);
    }

    public void create(Menu menu, List<Submenu> submenuList) {
        getEntityManager().persist(menu);
        List<MenuSubmenu> listaMenuSubmenu = menuSubmenuEJB.obtenerListaMenuSubmenu(menu);
        List<Submenu> submenuListAux = new ArrayList<>();
        List<Submenu> aAgregar = new ArrayList<>();
        List<Submenu> aEliminar = new ArrayList<>();
        for (MenuSubmenu m : listaMenuSubmenu) {
            if (!submenuListAux.contains(m.getIdSubmenu())) {
                submenuListAux.add(m.getIdSubmenu());
            }
        }

        for (Submenu s : submenuListAux) {
            if (!submenuList.contains(s)) {
                aEliminar.add(s);
            }
        }

        for (Submenu s : submenuList) {
            if (!submenuListAux.contains(s)) {
                aAgregar.add(s);
            }
        }

        for (Submenu s : aEliminar) {
            MenuSubmenu eliminar = menuSubmenuEJB.obtenerMenuSubmenu(menu, s);
            if (eliminar != null) {
                menuSubmenuEJB.remove(eliminar);
            }

        }

        for (Submenu s : aAgregar) {
            MenuSubmenu m = new MenuSubmenu();
            m.setIdMenu(menu);
            m.setIdSubmenu(s);
            menuSubmenuEJB.create(m);
        }

    }

    public void edit(Menu menu, List<Submenu> submenuList) {
        getEntityManager().merge(menu);
        List<MenuSubmenu> listaMenuSubmenu = menuSubmenuEJB.obtenerListaMenuSubmenu(menu);
        List<Submenu> submenuListAux = new ArrayList<>();
        List<Submenu> aAgregar = new ArrayList<>();
        List<Submenu> aEliminar = new ArrayList<>();
        for (MenuSubmenu m : listaMenuSubmenu) {
            if (!submenuListAux.contains(m.getIdSubmenu())) {
                submenuListAux.add(m.getIdSubmenu());
            }
        }

        for (Submenu s : submenuListAux) {
            if (!submenuList.contains(s)) {
                aEliminar.add(s);
            }
        }

        for (Submenu s : submenuList) {
            if (!submenuListAux.contains(s)) {
                aAgregar.add(s);
            }
        }

        for (Submenu s : aEliminar) {
            MenuSubmenu eliminar = menuSubmenuEJB.obtenerMenuSubmenu(menu, s);
            if (eliminar != null) {
                menuSubmenuEJB.remove(eliminar);
            }

        }

        for (Submenu s : aAgregar) {
            MenuSubmenu m = new MenuSubmenu();
            m.setIdMenu(menu);
            m.setIdSubmenu(s);
            menuSubmenuEJB.create(m);
        }

    }

    public Map<Menu, List<Submenu>> obtenerMenuUsuarioLogueado() {
        List<Submenu> listaSubmenu = submenuEjb.obtenerSubmenu();
        Map<Menu, List<Submenu>> mapAux = new HashMap<>();
        List<Submenu> listaSubmenuAux = new ArrayList<>();
        List<MenuSubmenu> listaMenusAux = new ArrayList<>();

        Query q = em.createQuery("Select s from MenuSubmenu s where s.idSubmenu in :listaSubmenu");
        q.setParameter("listaSubmenu", listaSubmenu);
        listaMenusAux = q.getResultList();

        for (MenuSubmenu ms : listaMenusAux) {
            if (!mapAux.containsKey(ms.getIdMenu())) {

                mapAux.put(ms.getIdMenu(), new ArrayList<Submenu>());
                mapAux.get(ms.getIdMenu()).add(ms.getIdSubmenu());
            } else {
                mapAux.get(ms.getIdMenu()).add(ms.getIdSubmenu());
            }
        }
        return mapAux;
    }

}
