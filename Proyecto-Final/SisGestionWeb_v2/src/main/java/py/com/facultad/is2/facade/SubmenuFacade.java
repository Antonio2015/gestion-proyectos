/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.facultad.is2.model.Menu;
import py.com.facultad.is2.model.MenuSubmenu;
import py.com.facultad.is2.model.Pantallas;
import py.com.facultad.is2.model.Rolespantalla;
import py.com.facultad.is2.model.Submenu;
import py.com.facultad.is2.model.Usuario;
import py.com.facultad.is2.view.LoginBean;

/**
 *
 * @author victoro
 */
@Stateless
public class SubmenuFacade extends AbstractFacade<Submenu> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @EJB
    private RolespantallaFacade rolesPantallaEjb;
    
    @Inject
    LoginBean loginBean;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SubmenuFacade() {
        super(Submenu.class);
    }

    public List<Submenu> obtenerSubmenu() {
        Usuario usuarioLogueado = loginBean.getUsuarioLogueado();
        List<Pantallas> listaPantallas = new ArrayList<>();
        List<Submenu> listaSubmenu = new ArrayList<>();
        List<Rolespantalla> listaRolesPantalla = rolesPantallaEjb.obtenerRolesPantalla(usuarioLogueado.getIdRol().getIdRol());
        for (Rolespantalla r : listaRolesPantalla) {
            listaPantallas.add(r.getIdPantalla());
        }

        try {
            Query q = em.createQuery("Select s from Submenu s where s.idPantalla in :listaPantallas");
            q.setParameter("listaPantallas", listaPantallas);
            listaSubmenu = q.getResultList();

        } catch (Exception e) {

        }
        return listaSubmenu;

    }

    public List<Submenu> subMenuSource(Menu menuSeleccionado) {
        List<Submenu> submenuList = super.findAll();
        List<Submenu> submenuAuxList = new ArrayList<>();
        List<Submenu> submenuReturn = new ArrayList<>();

        List<MenuSubmenu> menuSubmenuList = new ArrayList<>();
        try {

            Query q = em.createQuery("Select m from MenuSubmenu m where m.idMenu = :menuSeleccionado")
                    .setParameter("menuSeleccionado", menuSeleccionado);
            menuSubmenuList = q.getResultList();

            for (MenuSubmenu m : menuSubmenuList) {
                submenuAuxList.add(m.getIdSubmenu());
            }

            for (Submenu s : submenuList) {
                if (!submenuAuxList.contains(s)) {
                    submenuReturn.add(s);
                }
            }

        } catch (Exception e) {
            System.out.println("");

        }
        return submenuReturn;
    }

    public List<Submenu> subMenuTarget(Menu menuSeleccionado) {
        List<Submenu> submenuList = super.findAll();
        List<Submenu> submenuAuxList = new ArrayList<>();
        List<Submenu> submenuReturn = new ArrayList<>();

        List<MenuSubmenu> menuSubmenuList = new ArrayList<>();
        try {

            Query q = em.createQuery("Select m from MenuSubmenu m where m.idMenu = :menuSeleccionado")
                    .setParameter("menuSeleccionado", menuSeleccionado);
            menuSubmenuList = q.getResultList();

            for (MenuSubmenu m : menuSubmenuList) {
                submenuAuxList.add(m.getIdSubmenu());
            }

            for (Submenu s : submenuList) {
                if (submenuAuxList.contains(s)) {
                    submenuReturn.add(s);
                }
            }

        } catch (Exception e) {
            System.out.println("");

        }
        return submenuReturn;
    }

    public void guardar(List<Submenu> target) {
        for (Submenu s : target) {
            super.create(s);
        }

    }

}
