/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.facultad.is2.model.Estado;

/**
 *
 * @author victoro
 */
@Stateless
public class EstadoFacade extends AbstractFacade<Estado> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstadoFacade() {
        super(Estado.class);
    }

    public List<Estado> estadosSprint() {
        List<Estado> list = new ArrayList<>();

        try {
            Query q = em.createQuery("Select e from Estado e where e.codigo in ('TER', 'PEN', 'CUR')");
            list = q.getResultList();

            list = q.getResultList();
        } catch (Exception e) {

        }
        return list;
    }

    public Estado obtenerEstado(String estado) {
        Estado retorno = null;

        try {
            Query q = em.createQuery("Select e from Estado e where e.codigo = :cod", Estado.class)
                    .setParameter("cod", estado);
            retorno = (Estado) q.getSingleResult();

        } catch (Exception e) {

        }
        return retorno;
    }

}
