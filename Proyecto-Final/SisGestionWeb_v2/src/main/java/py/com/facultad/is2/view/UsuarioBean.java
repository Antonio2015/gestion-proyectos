/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;
import py.com.facultad.is2.facade.RolesFacade;
import py.com.facultad.is2.facade.UsuarioFacade;
import py.com.facultad.is2.model.Roles;
import py.com.facultad.is2.model.Usuario;

/**
 *
 * @author victoro
 */
@ManagedBean
@SessionScoped
public class UsuarioBean extends AbstractBean implements Serializable {

    @EJB
    private UsuarioFacade usuarioEJB;
    @EJB
    private RolesFacade rolEJB;
    private List<Usuario> listaUsuarios = new ArrayList<>();
    private Usuario usuarioSeleccionado;
    private Roles rolSeleccionado;
    private List<Roles> listaRoles = new ArrayList<>();
    private boolean editando;

    @PostConstruct
    public void init() {
        listaUsuarios = usuarioEJB.findAll();
        listaRoles = rolEJB.findAll();
        usuarioSeleccionado = new Usuario();
    }

    public void guardar() {
        if (rolSeleccionado != null) {
            try {
                usuarioSeleccionado.setIdRol(rolSeleccionado);
                usuarioSeleccionado.setPassword("12345");

                usuarioEJB.create(usuarioSeleccionado);
                infoMessage("Se guardó correctamente.");
                listaUsuarios = usuarioEJB.findAll();
                resetearValores();
                RequestContext.getCurrentInstance().execute("PF('wbUsuarios').hide()");
            } catch (Exception e) {
                errorMessage("Se produjo un error.");
            }
        }

    }

    public void agregarUsuario() {
        resetearValores();
        listaUsuarios = usuarioEJB.findAll();
        listaRoles = rolEJB.findAll();
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public Usuario getUsuarioSeleccionado() {
        return usuarioSeleccionado;
    }

    public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }

    public Roles getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(Roles rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }

    public List<Roles> getListaRoles() {
        return listaRoles;
    }

    public void setListaRoles(List<Roles> listaRoles) {
        this.listaRoles = listaRoles;
    }

    public boolean isEditando() {
        return editando;
    }

    public void setEditando(boolean editando) {
        this.editando = editando;
    }

    @Override
    public void resetearValores() {
        usuarioSeleccionado = new Usuario();
        rolSeleccionado = null;
        editando = false;
    }

    @Override
    public void inicializarListas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void antesActualizar() {
        listaUsuarios = usuarioEJB.findAll();
        listaRoles = rolEJB.findAll();
        rolSeleccionado = usuarioSeleccionado.getIdRol();
        editando = true;
    }

    @Override
    public void actualizar() {
        try {
            usuarioSeleccionado.setIdRol(rolSeleccionado);
            usuarioSeleccionado.setPassword("12345");

            usuarioEJB.edit(usuarioSeleccionado);
            infoMessage("Se actualizó correctamente.");
            listaUsuarios = usuarioEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbUsuarios').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void eliminar() {
        try {
            usuarioEJB.remove(usuarioSeleccionado);
            infoMessage("Eliminado correctamente");
            listaUsuarios = usuarioEJB.findAll();
        } catch (Exception e) {
            errorMessage("No se pudo eliminar el registro");
        }
    }

}
