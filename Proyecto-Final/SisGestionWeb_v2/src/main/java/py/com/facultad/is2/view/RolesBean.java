/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.view;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import org.primefaces.context.RequestContext;
import py.com.facultad.is2.facade.RolesFacade;
import py.com.facultad.is2.model.Roles;

@Named(value = "rolesBean")
@SessionScoped
public class RolesBean extends AbstractBean implements Serializable {

    private String paginaPanel;
    private List<Roles> listaRoles = new ArrayList<>();
    private Roles rolSeleccionado;
    private boolean editando;

    @EJB
    private RolesFacade rolEJB;

    public RolesBean() {
    }

    @PostConstruct
    public void init() {
        rolSeleccionado = new Roles();
        listaRoles = rolEJB.findAll();
    }

    public void agregarRoles() {
        resetearValores();

    }

    public String getPaginaPanel() {
        return paginaPanel;
    }

    public void setPaginaPanel(String paginaPanel) {
        this.paginaPanel = paginaPanel;
    }

    public List<Roles> getListaRoles() {
        return listaRoles;
    }

    public void setListaRoles(List<Roles> listaRoles) {
        this.listaRoles = listaRoles;
    }

    @Override
    public void resetearValores() {
        editando = false;
        rolSeleccionado = new Roles();
    }

    @Override
    public void inicializarListas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void guardar() {
        try {
            rolEJB.create(rolSeleccionado);
            infoMessage("Se guardó correctamente.");
            listaRoles = rolEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbRoles').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void actualizar() {
        try {
            rolEJB.edit(rolSeleccionado);
            infoMessage("Se actualizó correctamente.");
            listaRoles = rolEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbRoles').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void eliminar() {
        try {
            rolEJB.remove(rolSeleccionado);
            infoMessage("Eliminado correctamente");
            listaRoles = rolEJB.findAll();
        } catch (Exception e) {
            errorMessage("No se pudo eliminar el registro");
        }
    }

    @Override
    public void antesActualizar() {
        editando = true;
    }

    public Roles getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(Roles rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }

    public boolean isEditando() {
        return editando;
    }

    public void setEditando(boolean editando) {
        this.editando = editando;
    }

}
