/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author victoro
 */
@Entity
@Table(name = "sprint_det")
@NamedQueries({
    @NamedQuery(name = "SprintDet.findAll", query = "SELECT s FROM SprintDet s")
    , @NamedQuery(name = "SprintDet.findByIdSprintDet", query = "SELECT s FROM SprintDet s WHERE s.idSprintDet = :idSprintDet")})
public class SprintDet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_sprint_det", nullable = false)
    private Integer idSprintDet;
    @JoinColumn(name = "id_sprint", referencedColumnName = "id_sprint")
    @ManyToOne
    private Sprint idSprint;
    @JoinColumn(name = "id_userstory", referencedColumnName = "id_userstory")
    @ManyToOne
    private Userstory idUserstory;

    public SprintDet() {
    }

    public SprintDet(Integer idSprintDet) {
        this.idSprintDet = idSprintDet;
    }

    public Integer getIdSprintDet() {
        return idSprintDet;
    }

    public void setIdSprintDet(Integer idSprintDet) {
        this.idSprintDet = idSprintDet;
    }

    public Sprint getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(Sprint idSprint) {
        this.idSprint = idSprint;
    }

    public Userstory getIdUserstory() {
        return idUserstory;
    }

    public void setIdUserstory(Userstory idUserstory) {
        this.idUserstory = idUserstory;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSprintDet != null ? idSprintDet.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SprintDet)) {
            return false;
        }
        SprintDet other = (SprintDet) object;
        if ((this.idSprintDet == null && other.idSprintDet != null) || (this.idSprintDet != null && !this.idSprintDet.equals(other.idSprintDet))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.facultad.is2.model.SprintDet[ idSprintDet=" + idSprintDet + " ]";
    }
    
}
