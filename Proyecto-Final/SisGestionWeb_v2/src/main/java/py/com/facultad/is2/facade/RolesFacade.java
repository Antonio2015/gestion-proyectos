/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.facultad.is2.model.Pantallas;
import py.com.facultad.is2.model.Roles;
import py.com.facultad.is2.model.Rolespantalla;

/**
 *
 * @author victoro
 */
@Stateless
public class RolesFacade extends AbstractFacade<Roles> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RolesFacade() {
        super(Roles.class);
    }

    public List<Roles> rolesSource(Pantallas pantallaSeleccionada) {
        List<Roles> rolesList = super.findAll();
        List<Roles> rolesAuxList = new ArrayList<>();
        List<Roles> rolesReturn = new ArrayList<>();

        List<Rolespantalla> rolesPantallasList = new ArrayList<>();
        try {

            Query q = em.createQuery("Select r from Rolespantalla r where r.idPantalla = :pantallaSeleccionada")
                    .setParameter("pantallaSeleccionada", pantallaSeleccionada);
            rolesPantallasList = q.getResultList();

            for (Rolespantalla r : rolesPantallasList) {
                rolesAuxList.add(r.getIdRol());
            }

            for (Roles r : rolesList) {
                if (!rolesAuxList.contains(r)) {
                    rolesReturn.add(r);
                }
            }

        } catch (Exception e) {
            System.out.println("");

        }
        return rolesReturn;
    }
    
     public List<Roles> rolesTarget(Pantallas pantallaSeleccionada) {
        List<Roles> rolesList = super.findAll();
        List<Roles> rolesAuxList = new ArrayList<>();
        List<Roles> rolesReturn = new ArrayList<>();

        List<Rolespantalla> rolesPantallasList = new ArrayList<>();
        try {

            Query q = em.createQuery("Select r from Rolespantalla r where r.idPantalla = :pantallaSeleccionada")
                    .setParameter("pantallaSeleccionada", pantallaSeleccionada);
            rolesPantallasList = q.getResultList();

            for (Rolespantalla r : rolesPantallasList) {
                rolesAuxList.add(r.getIdRol());
            }

            for (Roles r : rolesList) {
                if (rolesAuxList.contains(r)) {
                    rolesReturn.add(r);
                }
            }

        } catch (Exception e) {
            System.out.println("");

        }
        return rolesReturn;
    }

}
