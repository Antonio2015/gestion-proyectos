/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;
import py.com.facultad.is2.facade.BacklogFacade;
import py.com.facultad.is2.facade.EquipoFacade;
import py.com.facultad.is2.facade.EstadoFacade;
import py.com.facultad.is2.facade.ProyectoFacade;
import py.com.facultad.is2.model.Backlog;
import py.com.facultad.is2.model.Equipo;
import py.com.facultad.is2.model.Estado;
import py.com.facultad.is2.model.Proyecto;

/**
 *
 * @author victoro
 */
@ManagedBean
@SessionScoped
public class ProyectosBean extends AbstractBean implements Serializable {

    private List<Proyecto> listaProyectos = new ArrayList<>();
    private List<Estado> listaEstados = new ArrayList<>();
    private List<Backlog> listaBacklog = new ArrayList<>();
    private List<Equipo> listaEquipo = new ArrayList<>();
    private Proyecto proyectoSeleccionado;
    private Equipo equipoSeleccionado;
    private Estado estadoSeleccionado;
    private Backlog backlogSeleccionado;
    private boolean editando;

    @EJB
    private ProyectoFacade proyectoEJB;
    @EJB
    private EstadoFacade estadoEJB;
    @EJB
    private BacklogFacade backlogEJB;
    @EJB
    private EquipoFacade equipoEJB;

    @PostConstruct
    public void init() {
        proyectoSeleccionado = new Proyecto();
        listaProyectos = proyectoEJB.findAll();
        listaEstados = estadoEJB.findAll();
        listaBacklog = backlogEJB.findAll();
        listaEquipo = equipoEJB.findAll();
    }

    @Override
    public void resetearValores() {
        backlogSeleccionado = null;
        equipoSeleccionado = null;
        estadoSeleccionado = null;
        proyectoSeleccionado = new Proyecto();
        editando = false;
    }

    @Override
    public void inicializarListas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void guardar() {
        try {
            proyectoSeleccionado.setIdEstado(estadoSeleccionado);
            proyectoSeleccionado.setIdEquipo(equipoSeleccionado);

            proyectoEJB.create(proyectoSeleccionado);
            infoMessage("Se guardó correctamente.");
            listaProyectos = proyectoEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbProyectos').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void antesActualizar() {
        equipoSeleccionado = proyectoSeleccionado.getIdEquipo();
        estadoSeleccionado = proyectoSeleccionado.getIdEstado();
        editando = true;
        listaProyectos = proyectoEJB.findAll();
        listaEstados = estadoEJB.findAll();
        listaBacklog = backlogEJB.findAll();
        listaEquipo = equipoEJB.findAll();
    }

    @Override
    public void actualizar() {
        try {
            proyectoSeleccionado.setIdEstado(estadoSeleccionado);
            proyectoSeleccionado.setIdEquipo(equipoSeleccionado);

            proyectoEJB.edit(proyectoSeleccionado);
            infoMessage("Se actualizó correctamente.");
            listaProyectos = proyectoEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbProyectos').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void eliminar() {
        try {
            proyectoSeleccionado.setInactivo(true);
            proyectoEJB.edit(proyectoSeleccionado);
            infoMessage("Eliminado correctamente");
            listaProyectos = proyectoEJB.findAll();
        } catch (Exception e) {
            errorMessage("No se pudo eliminar el registro");
        }

    }

    public void agregarProyecto() {
        resetearValores();
        listaProyectos = proyectoEJB.findAll();
        listaEstados = estadoEJB.findAll();
        listaBacklog = backlogEJB.findAll();
        listaEquipo = equipoEJB.findAll();
    }

    public List<Proyecto> getListaProyectos() {
        return listaProyectos;
    }

    public void setListaProyectos(List<Proyecto> listaProyectos) {
        this.listaProyectos = listaProyectos;
    }

    public List<Estado> getListaEstados() {
        return listaEstados;
    }

    public void setListaEstados(List<Estado> listaEstados) {
        this.listaEstados = listaEstados;
    }

    public List<Backlog> getListaBacklog() {
        return listaBacklog;
    }

    public void setListaBacklog(List<Backlog> listaBacklog) {
        this.listaBacklog = listaBacklog;
    }

    public List<Equipo> getListaEquipo() {
        return listaEquipo;
    }

    public void setListaEquipo(List<Equipo> listaEquipo) {
        this.listaEquipo = listaEquipo;
    }

    public Proyecto getProyectoSeleccionado() {
        return proyectoSeleccionado;
    }

    public void setProyectoSeleccionado(Proyecto proyectoSeleccionado) {
        this.proyectoSeleccionado = proyectoSeleccionado;
    }

    public Equipo getEquipoSeleccionado() {
        return equipoSeleccionado;
    }

    public void setEquipoSeleccionado(Equipo equipoSeleccionado) {
        this.equipoSeleccionado = equipoSeleccionado;
    }

    public Estado getEstadoSeleccionado() {
        return estadoSeleccionado;
    }

    public void setEstadoSeleccionado(Estado estadoSeleccionado) {
        this.estadoSeleccionado = estadoSeleccionado;
    }

    public Backlog getBacklogSeleccionado() {
        return backlogSeleccionado;
    }

    public void setBacklogSeleccionado(Backlog backlogSeleccionado) {
        this.backlogSeleccionado = backlogSeleccionado;
    }

    public boolean isEditando() {
        return editando;
    }

    public void setEditando(boolean editando) {
        this.editando = editando;
    }

}
