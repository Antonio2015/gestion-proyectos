/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.facultad.is2.model.Backlog;
import py.com.facultad.is2.model.Estado;
import py.com.facultad.is2.model.Sprint;
import py.com.facultad.is2.model.SprintDet;
import py.com.facultad.is2.model.Userstory;

/**
 *
 * @author victoro
 */
@Stateless
public class UserstoryFacade extends AbstractFacade<Userstory> {

    @EJB
    EstadoFacade estadosEJB;

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserstoryFacade() {
        super(Userstory.class);
    }

    public List<Userstory> findByBacklog(Backlog backlogSeleccionado) {
        List<Userstory> userStoryList = new ArrayList<>();
        try {
            Query q = em.createQuery("Select s from Userstory s where s.idBacklog = :backlog")
                    .setParameter("backlog", backlogSeleccionado);
            userStoryList = q.getResultList();
        } catch (Exception e) {

        }

        return userStoryList;
    }

    public List<Userstory> usSources(Backlog backlogSeleccionado) {
        List<Userstory> usList = super.findAll();
        List<Userstory> usAuxList = new ArrayList<>();
        List<Userstory> usReturnList = new ArrayList<>();
        List<SprintDet> sprintDetList = new ArrayList<>();
        List<Userstory> userStoryList = new ArrayList<>();
        List<Userstory> userStorySeleccionados = new ArrayList<>();
        try {

            Query q = em.createQuery("Select u from Userstory u where u.idBacklog = :back")
                    .setParameter("back", backlogSeleccionado);
            userStoryList = q.getResultList();

            Query q2 = em.createQuery("Select s from SprintDet s");
            sprintDetList = q2.getResultList();

            for (SprintDet r : sprintDetList) {
                userStorySeleccionados.add(r.getIdUserstory());
            }

            for (Userstory u : userStoryList) {
                if (!userStorySeleccionados.contains(u)) {
                    usReturnList.add(u);
                }
            }

        } catch (Exception e) {
            System.out.println("");

        }
        return usReturnList;
    }

    public List<Userstory> usTarget(Sprint sprintSeleccionado) {
        List<Userstory> usList = super.findAll();
        List<Userstory> usAuxList = new ArrayList<>();
        List<Userstory> usReturnList = new ArrayList<>();

        List<SprintDet> sprintDetList = new ArrayList<>();
        try {

            Query q = em.createQuery("Select s from SprintDet s where s.idSprint = :sprint")
                    .setParameter("sprint", sprintSeleccionado);
            sprintDetList = q.getResultList();

            for (SprintDet r : sprintDetList) {
                usAuxList.add(r.getIdUserstory());
            }

            for (Userstory u : usList) {
                if (usAuxList.contains(u)) {
                    usReturnList.add(u);
                }
            }

        } catch (Exception e) {
            System.out.println("");

        }
        return usReturnList;
    }

    public List<Userstory> usSources(Backlog backlogSeleccionado, Sprint sprintSeleccionado) {
        List<Userstory> usList = super.findAll();
        List<Userstory> usAuxList = new ArrayList<>();
        List<Userstory> usReturnList = new ArrayList<>();
        List<SprintDet> sprintDetList = new ArrayList<>();
        List<SprintDet> sprintDetList2 = new ArrayList<>();
        List<Userstory> userStoryList = new ArrayList<>();
        List<Userstory> userStoryAuxList = new ArrayList<>();
        List<Userstory> userStoryAuxList2 = new ArrayList<>();
        List<Userstory> userStorySeleccionados = new ArrayList<>();
        try {

            Query q = em.createQuery("Select u from Userstory u where u.idBacklog = :back")
                    .setParameter("back", backlogSeleccionado);
            userStoryList = q.getResultList();

            Query q2 = em.createQuery("Select s from SprintDet s where s.idSprint = :sprint")
                    .setParameter("sprint", sprintSeleccionado);
            sprintDetList = q2.getResultList();

            Query q3 = em.createQuery("Select s from SprintDet s");
            sprintDetList2 = q3.getResultList();

            for (SprintDet sp : sprintDetList2) {
                userStoryAuxList2.add(sp.getIdUserstory());
            }

            for (Userstory u : userStoryList) {
                if (!userStoryAuxList2.contains(u)) {
                    userStoryAuxList.add(u);
                }
            }

            for (SprintDet r : sprintDetList) {
                userStorySeleccionados.add(r.getIdUserstory());
            }

            for (Userstory u : userStoryAuxList) {
                if (!userStorySeleccionados.contains(u)) {
                    usReturnList.add(u);
                }
            }

        } catch (Exception e) {
            System.out.println("");

        }
        return usReturnList;
    }

    public void actualizarEstado(Userstory us, String estadoUS) {
        Estado estado = null;

        try {
            estado = estadosEJB.obtenerEstado(estadoUS);
            if (estado != null) {
                us.setIdEstado(estado);
                super.edit(us);
            }
        } catch (Exception e) {

        }

    }

}
