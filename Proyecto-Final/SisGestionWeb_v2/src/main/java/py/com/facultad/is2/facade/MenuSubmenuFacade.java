/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.facultad.is2.model.Menu;
import py.com.facultad.is2.model.MenuSubmenu;
import py.com.facultad.is2.model.Submenu;

/**
 *
 * @author victoro
 */
@Stateless
public class MenuSubmenuFacade extends AbstractFacade<MenuSubmenu> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MenuSubmenuFacade() {
        super(MenuSubmenu.class);
    }

    public MenuSubmenu obtenerMenuSubmenu(Menu menu, Submenu submenu) {
        MenuSubmenu m = null;
        try {
            Query q = em.createQuery("Select m from MenuSubmenu m where m.idMenu = :menu and m.idSubmenu = :submenu", MenuSubmenu.class)
                    .setParameter("menu", menu)
                    .setParameter("submenu", submenu);
            m = (MenuSubmenu) q.getSingleResult();
        } catch (Exception e) {

        }

        return m;

    }
    
     public List<MenuSubmenu> obtenerListaMenuSubmenu(Menu menu) {
        List<MenuSubmenu> menuSubmenuList = new ArrayList<>();
        try {
            Query q = em.createQuery("Select m from MenuSubmenu m where m.idMenu = :menu")
                    .setParameter("menu", menu);
                    
            menuSubmenuList =  q.getResultList();
        } catch (Exception e) {

        }

        return menuSubmenuList;

    }

}
