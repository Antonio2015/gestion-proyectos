/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author victoro
 */
@Entity
@Table(name = "pantallas")
@NamedQueries({
    @NamedQuery(name = "Pantallas.findAll", query = "SELECT p FROM Pantallas p")
    , @NamedQuery(name = "Pantallas.findByIdPantalla", query = "SELECT p FROM Pantallas p WHERE p.idPantalla = :idPantalla")
    , @NamedQuery(name = "Pantallas.findByNombre", query = "SELECT p FROM Pantallas p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Pantallas.findByUrl", query = "SELECT p FROM Pantallas p WHERE p.url = :url")})
public class Pantallas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_pantalla", nullable = false)
    private Integer idPantalla;
    @Size(max = 100)
    @Column(name = "nombre", length = 100)
    private String nombre;
    @Size(max = 100)
    @Column(name = "url", length = 100)
    private String url;
    @OneToMany(mappedBy = "idPantalla")
    private List<Rolespantalla> rolespantallaList;
    @OneToMany(mappedBy = "idPantalla")
    private List<Submenu> submenuList;

    public Pantallas() {
    }

    public Pantallas(Integer idPantalla) {
        this.idPantalla = idPantalla;
    }

    public Integer getIdPantalla() {
        return idPantalla;
    }

    public void setIdPantalla(Integer idPantalla) {
        this.idPantalla = idPantalla;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Rolespantalla> getRolespantallaList() {
        return rolespantallaList;
    }

    public void setRolespantallaList(List<Rolespantalla> rolespantallaList) {
        this.rolespantallaList = rolespantallaList;
    }

    public List<Submenu> getSubmenuList() {
        return submenuList;
    }

    public void setSubmenuList(List<Submenu> submenuList) {
        this.submenuList = submenuList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPantalla != null ? idPantalla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pantallas)) {
            return false;
        }
        Pantallas other = (Pantallas) object;
        if ((this.idPantalla == null && other.idPantalla != null) || (this.idPantalla != null && !this.idPantalla.equals(other.idPantalla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.facultad.is2.model.Pantallas[ idPantalla=" + idPantalla + " ]";
    }
    
}
