/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;
import py.com.facultad.is2.facade.PantallasFacade;
import py.com.facultad.is2.facade.SubmenuFacade;
import py.com.facultad.is2.model.Pantallas;
import py.com.facultad.is2.model.Submenu;

/**
 *
 * @author victoro
 */
@ManagedBean
@SessionScoped
public class SubmenuBean extends AbstractBean implements Serializable {

    private List<Pantallas> listaPantallas = new ArrayList<>();
    private List<Submenu> listaSubmenu = new ArrayList<>();

    private Pantallas pantallaSeleccionada;
    private Submenu submenuSeleccionado;
    private boolean editando;

    @EJB
    private PantallasFacade pantallasEJB;
    @EJB
    private SubmenuFacade submenuEJB;

    @PostConstruct
    public void init() {
        submenuSeleccionado = new Submenu();
        listaSubmenu = submenuEJB.findAll();
        listaPantallas = pantallasEJB.findAll();
    }

    @Override
    public void resetearValores() {
        pantallaSeleccionada = null;
        submenuSeleccionado = new Submenu();
        editando = false;
    }

    @Override
    public void inicializarListas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void guardar() {
        try {
            submenuSeleccionado.setIdPantalla(pantallaSeleccionada);

            submenuEJB.create(submenuSeleccionado);
            infoMessage("Se guardó correctamente.");
            listaSubmenu = submenuEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbSubmenu').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void antesActualizar() {
        pantallaSeleccionada = submenuSeleccionado.getIdPantalla();
        editando = true;
        listaSubmenu = submenuEJB.findAll();
        listaPantallas = pantallasEJB.findAll();
    }

    @Override
    public void actualizar() {
        try {
            submenuSeleccionado.setIdPantalla(pantallaSeleccionada);

            submenuEJB.edit(submenuSeleccionado);
            infoMessage("Se actualizó correctamente.");
            listaSubmenu = submenuEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbSubmenu').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void eliminar() {
        try {
            submenuEJB.remove(submenuSeleccionado);
            infoMessage("Eliminado correctamente");
            listaSubmenu = submenuEJB.findAll();
        } catch (Exception e) {
            errorMessage("No se pudo eliminar el registro");
        }
    }

    public void agregarProyecto() {
        resetearValores();
        listaSubmenu = submenuEJB.findAll();
        listaPantallas = pantallasEJB.findAll();
    }

    public List<Pantallas> getListaPantallas() {
        return listaPantallas;
    }

    public void setListaPantallas(List<Pantallas> listaPantallas) {
        this.listaPantallas = listaPantallas;
    }

    public List<Submenu> getListaSubmenu() {
        return listaSubmenu;
    }

    public void setListaSubmenu(List<Submenu> listaSubmenu) {
        this.listaSubmenu = listaSubmenu;
    }

    public Pantallas getPantallaSeleccionada() {
        return pantallaSeleccionada;
    }

    public void setPantallaSeleccionada(Pantallas pantallaSeleccionada) {
        this.pantallaSeleccionada = pantallaSeleccionada;
    }

    public Submenu getSubmenuSeleccionado() {
        return submenuSeleccionado;
    }

    public void setSubmenuSeleccionado(Submenu submenuSeleccionado) {
        this.submenuSeleccionado = submenuSeleccionado;
    }

    public boolean isEditando() {
        return editando;
    }

    public void setEditando(boolean editando) {
        this.editando = editando;
    }

}
