/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author victoro
 */
@Entity
@Table(name = "submenu")
@NamedQueries({
    @NamedQuery(name = "Submenu.findAll", query = "SELECT s FROM Submenu s")
    , @NamedQuery(name = "Submenu.findByIdSubmenu", query = "SELECT s FROM Submenu s WHERE s.idSubmenu = :idSubmenu")
    , @NamedQuery(name = "Submenu.findByNombre", query = "SELECT s FROM Submenu s WHERE s.nombre = :nombre")})
public class Submenu implements Serializable {

    @OneToMany(mappedBy = "idSubmenu")
    private List<MenuSubmenu> menuSubmenuList;

    @Size(max = 30)
    @Column(name = "icono", length = 30)
    private String icono;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_submenu", nullable = false)
    private Integer idSubmenu;
    @Size(max = 100)
    @Column(name = "nombre", length = 100)
    private String nombre;
    @JoinColumn(name = "id_pantalla", referencedColumnName = "id_pantalla")
    @ManyToOne
    private Pantallas idPantalla;

    public Submenu() {
    }

    public Submenu(Integer idSubmenu) {
        this.idSubmenu = idSubmenu;
    }

    public Integer getIdSubmenu() {
        return idSubmenu;
    }

    public void setIdSubmenu(Integer idSubmenu) {
        this.idSubmenu = idSubmenu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public Pantallas getIdPantalla() {
        return idPantalla;
    }

    public void setIdPantalla(Pantallas idPantalla) {
        this.idPantalla = idPantalla;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubmenu != null ? idSubmenu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Submenu)) {
            return false;
        }
        Submenu other = (Submenu) object;
        if ((this.idSubmenu == null && other.idSubmenu != null) || (this.idSubmenu != null && !this.idSubmenu.equals(other.idSubmenu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.facultad.is2.model.Submenu[ idSubmenu=" + idSubmenu + " ]";
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public List<MenuSubmenu> getMenuSubmenuList() {
        return menuSubmenuList;
    }

    public void setMenuSubmenuList(List<MenuSubmenu> menuSubmenuList) {
        this.menuSubmenuList = menuSubmenuList;
    }
    
}
