/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.com.facultad.is2.model.ProyectoClientes;

/**
 *
 * @author victoro
 */
@Stateless
public class ProyectoClientesFacade extends AbstractFacade<ProyectoClientes> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProyectoClientesFacade() {
        super(ProyectoClientes.class);
    }
    
}
