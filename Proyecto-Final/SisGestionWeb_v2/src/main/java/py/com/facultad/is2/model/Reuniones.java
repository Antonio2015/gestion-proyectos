/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author victoro
 */
@Entity
@Table(name = "reuniones")
@NamedQueries({
    @NamedQuery(name = "Reuniones.findAll", query = "SELECT r FROM Reuniones r")
    , @NamedQuery(name = "Reuniones.findByIdReunion", query = "SELECT r FROM Reuniones r WHERE r.idReunion = :idReunion")
    , @NamedQuery(name = "Reuniones.findByFechaReunion", query = "SELECT r FROM Reuniones r WHERE r.fechaReunion = :fechaReunion")
    , @NamedQuery(name = "Reuniones.findByObservacion", query = "SELECT r FROM Reuniones r WHERE r.observacion = :observacion")})
public class Reuniones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_reunion", nullable = false)
    private Integer idReunion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_reunion", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaReunion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "observacion", nullable = false, length = 2147483647)
    private String observacion;
    @JoinColumn(name = "id_equipo", referencedColumnName = "id_equipo", nullable = false)
    @ManyToOne(optional = false)
    private Equipo idEquipo;

    public Reuniones() {
    }

    public Reuniones(Integer idReunion) {
        this.idReunion = idReunion;
    }

    public Reuniones(Integer idReunion, Date fechaReunion, String observacion) {
        this.idReunion = idReunion;
        this.fechaReunion = fechaReunion;
        this.observacion = observacion;
    }

    public Integer getIdReunion() {
        return idReunion;
    }

    public void setIdReunion(Integer idReunion) {
        this.idReunion = idReunion;
    }

    public Date getFechaReunion() {
        return fechaReunion;
    }

    public void setFechaReunion(Date fechaReunion) {
        this.fechaReunion = fechaReunion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Equipo getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Equipo idEquipo) {
        this.idEquipo = idEquipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReunion != null ? idReunion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reuniones)) {
            return false;
        }
        Reuniones other = (Reuniones) object;
        if ((this.idReunion == null && other.idReunion != null) || (this.idReunion != null && !this.idReunion.equals(other.idReunion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.facultad.is2.model.Reuniones[ idReunion=" + idReunion + " ]";
    }
    
}
