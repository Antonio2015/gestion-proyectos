/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.facultad.is2.model.Usuario;

/**
 *
 * @author victoro
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    public Usuario obtenerUsuario(String username) {
        Usuario retorno = null;
        try {
            Query q = em.createQuery("Select u from Usuario u where u.nombreUsuario = :usu")
                    .setParameter("usu", username);
            retorno = (Usuario) q.getSingleResult();

        } catch (Exception e) {

        }
        return retorno;

    }

}
