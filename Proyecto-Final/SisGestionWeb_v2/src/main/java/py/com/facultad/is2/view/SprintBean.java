/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;
import py.com.facultad.is2.facade.BacklogFacade;
import py.com.facultad.is2.facade.EstadoFacade;
import py.com.facultad.is2.facade.SprintFacade;
import py.com.facultad.is2.facade.UserstoryFacade;
import py.com.facultad.is2.model.Backlog;
import py.com.facultad.is2.model.Estado;
import py.com.facultad.is2.model.Roles;
import py.com.facultad.is2.model.Sprint;
import py.com.facultad.is2.model.Userstory;

/**
 *
 * @author victoro
 */
@ManagedBean
@SessionScoped
public class SprintBean extends AbstractBean implements Serializable {

    private List<Sprint> sprintList = new ArrayList<>();
    private List<Estado> estadosList = new ArrayList<>();
    private List<Userstory> userstoryList = new ArrayList<>();
    private List<Backlog> backlogList = new ArrayList<>();

    private DualListModel<Userstory> userStoryDualList;
    private Estado estadoSeleccionado;
    private Sprint sprintSeleccionado;
    private Backlog backlogSeleccionado;

    private boolean editando;

    @EJB
    EstadoFacade estadoEJB;

    @EJB
    BacklogFacade backlogEJB;

    @EJB
    UserstoryFacade userstoryEJB;

    @EJB
    SprintFacade sprintEJB;

    @Override
    public void init() {
        sprintSeleccionado = new Sprint();
        estadosList = estadoEJB.findAll();
        sprintList = sprintEJB.findAll();
        backlogList = backlogEJB.findAll();
        userStoryDualList = new DualListModel<Userstory>(new ArrayList<Userstory>(), new ArrayList<Userstory>());
    }

    @Override
    public void resetearValores() {
        sprintSeleccionado = new Sprint();
        estadoSeleccionado = null;
        backlogSeleccionado = null;
        userstoryList = new ArrayList<>();
        userStoryDualList = new DualListModel<Userstory>(new ArrayList<Userstory>(), new ArrayList<Userstory>());
        editando = false;
    }

    public void cargarListaUS() {
        List<Userstory> source = userstoryEJB.usSources(backlogSeleccionado, sprintSeleccionado);
        List<Userstory> target = userstoryEJB.usTarget(sprintSeleccionado);
        userStoryDualList = new DualListModel<Userstory>(source, target);
    }

    public void agregarSprint() {
        resetearValores();
        backlogList = backlogEJB.findAll();
    }

    @Override
    public void inicializarListas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//    @Override
    public void guardar() {
        try {
            sprintSeleccionado.setIdEstado(estadoSeleccionado);
            sprintSeleccionado.setIdBacklog(backlogSeleccionado);
            sprintEJB.create(sprintSeleccionado, userStoryDualList.getTarget());
            infoMessage("Se guardó correctamente.");
            sprintList = sprintEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbSprint').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void actualizar() {
        try {
            sprintSeleccionado.setIdEstado(estadoSeleccionado);
            sprintSeleccionado.setIdBacklog(backlogSeleccionado);
            sprintEJB.edit(sprintSeleccionado, userStoryDualList.getTarget());
            infoMessage("Se actualizó correctamente.");
            sprintList = sprintEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbSprint').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void eliminar() {
        try {
            sprintSeleccionado.setInactivo(true);
            sprintEJB.edit(sprintSeleccionado);
            infoMessage("Eliminado correctamente");
            sprintList = sprintEJB.findAll();
        } catch (Exception e) {
            errorMessage("No se pudo eliminar el registro");
        }
    }

    @Override
    public void antesActualizar() {
        estadoSeleccionado = sprintSeleccionado.getIdEstado();
        backlogSeleccionado = sprintSeleccionado.getIdBacklog();
        editando = true;
        cargarListaUS();
        backlogList = backlogEJB.findAll();
    }

    public boolean isEditando() {
        return editando;
    }

    public void setEditando(boolean editando) {
        this.editando = editando;
    }

    public List<Sprint> getSprintList() {
        return sprintList;
    }

    public void setSprintList(List<Sprint> sprintList) {
        this.sprintList = sprintList;
    }

    public List<Estado> getEstadosList() {
        return estadosList;
    }

    public void setEstadosList(List<Estado> estadosList) {
        this.estadosList = estadosList;
    }

    public Estado getEstadoSeleccionado() {
        return estadoSeleccionado;
    }

    public void setEstadoSeleccionado(Estado estadoSeleccionado) {
        this.estadoSeleccionado = estadoSeleccionado;
    }

    public Sprint getSprintSeleccionado() {
        return sprintSeleccionado;
    }

    public void setSprintSeleccionado(Sprint sprintSeleccionado) {
        this.sprintSeleccionado = sprintSeleccionado;
    }

    public void cargarUS() {

    }

    public List<Userstory> getUserstoryList() {
        return userstoryList;
    }

    public void setUserstoryList(List<Userstory> userstoryList) {
        this.userstoryList = userstoryList;
    }

    public DualListModel<Userstory> getUserStoryDualList() {
        return userStoryDualList;
    }

    public void setUserStoryDualList(DualListModel<Userstory> userStoryDualList) {
        this.userStoryDualList = userStoryDualList;
    }

    public Backlog getBacklogSeleccionado() {
        return backlogSeleccionado;
    }

    public void setBacklogSeleccionado(Backlog backlogSeleccionado) {
        this.backlogSeleccionado = backlogSeleccionado;
    }

    public List<Backlog> getBacklogList() {
        return backlogList;
    }

    public void setBacklogList(List<Backlog> backlogList) {
        this.backlogList = backlogList;
    }

    public void cargarPickList() {
        List<Userstory> source = userstoryEJB.usSources(backlogSeleccionado);
        List<Userstory> target = userstoryEJB.usTarget(sprintSeleccionado);
        userStoryDualList = new DualListModel<Userstory>(source, target);
    }

}
