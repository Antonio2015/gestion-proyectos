/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.view;

import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import py.com.facultad.is2.facade.MenuFacade;
import py.com.facultad.is2.facade.SubmenuFacade;
import py.com.facultad.is2.model.Menu;
import py.com.facultad.is2.model.Submenu;
import py.com.facultad.is2.model.Usuario;

@Named(value = "menuController")
@SessionScoped
public class MenuController extends AbstractBean implements Serializable {

    private List<Menu> listMenus;
    private Map<Menu, List<Submenu>> menuSubmenuList;
    private MenuModel model;
    private String paginaPanel;
    private List<Menu> listaMenu = new ArrayList<>();
    private List<Submenu> submenuList = new ArrayList<>();
    private Menu menuSeleccionado;
    private boolean editando;
    private DualListModel<Submenu> submenuDualList;
    private List<Submenu> target = new ArrayList<>();
    private List<Submenu> source = new ArrayList<>();
    private List<Submenu> aAgregar = new ArrayList<>();
    private List<Submenu> aEliminar = new ArrayList<>();

    @EJB
    private MenuFacade menuEJB;
    @EJB
    private SubmenuFacade submenuEJB;

//    @ManagedProperty(value = "#{loginBean}")
    @Inject
    LoginBean loginBean;

    public MenuController() {
    }

    @PostConstruct
    public void init() {
        Usuario usuarioLogueado = loginBean.getUsuarioLogueado();
        menuSeleccionado = new Menu();
        submenuDualList = new DualListModel<Submenu>(new ArrayList<Submenu>(), new ArrayList<Submenu>());
        cargarMenu();
    }

    private void cargarMenu() {
        listaMenu = menuEJB.findAll();
        menuSubmenuList = menuEJB.obtenerMenuUsuarioLogueado();
        model = new DefaultMenuModel();

        for (Map.Entry<Menu, List<Submenu>> entry : menuSubmenuList.entrySet()) {
            DefaultSubMenu firstSubmenu = new DefaultSubMenu(entry.getKey().getNombre());
            for (Submenu s : entry.getValue()) {

                DefaultMenuItem item = new DefaultMenuItem(s.getNombre());
                item.setUrl(s.getIdPantalla().getUrl());
                item.setIcon(s.getIcono());
                firstSubmenu.addElement(item);
            }
            model.addElement(firstSubmenu);
        }
    }

    public void agregarMenu() {
        resetearValores();
        List<Submenu> source = submenuEJB.findAll();
        List<Submenu> target = new ArrayList<>();

        submenuDualList = new DualListModel<Submenu>(source, target);
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public String getPaginaPanel() {
        return paginaPanel;
    }

    public void setPaginaPanel(String paginaPanel) {
        this.paginaPanel = paginaPanel;
    }

    public List<Menu> getListaMenu() {
        return listaMenu;
    }

    public void setListaMenu(List<Menu> listaMenu) {
        this.listaMenu = listaMenu;
    }

    @Override
    public void resetearValores() {
        editando = false;
        menuSeleccionado = new Menu();
        aAgregar = new ArrayList<>();
        aEliminar = new ArrayList<>();
    }

    @Override
    public void inicializarListas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void guardar() {
        try {
            menuEJB.create(menuSeleccionado, submenuDualList.getTarget());
            infoMessage("Se guardó correctamente.");
            cargarMenu();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbMenu').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void actualizar() {
        try {
            menuEJB.edit(menuSeleccionado, submenuDualList.getTarget());
            infoMessage("Se actualizó correctamente.");
            cargarMenu();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbMenu').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    public void cargarListaSubmenu() {
        source = submenuEJB.subMenuSource(menuSeleccionado);
        target = submenuEJB.subMenuTarget(menuSeleccionado);

        submenuDualList = new DualListModel<Submenu>(source, target);

    }

    public void guardarSubmenu() {
        try {
            submenuEJB.guardar(submenuDualList.getTarget());
            infoMessage("Se guardó correctamente.");
            cargarMenu();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbSubMenu').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void eliminar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void antesActualizar() {
        editando = true;
        cargarListaSubmenu();

    }

    public Menu getMenuSeleccionado() {
        return menuSeleccionado;
    }

    public void setMenuSeleccionado(Menu menuSeleccionado) {
        this.menuSeleccionado = menuSeleccionado;
    }

    public boolean isEditando() {
        return editando;
    }

    public void setEditando(boolean editando) {
        this.editando = editando;
    }

    public List<Submenu> getSubmenuList() {
        return submenuList;
    }

    public void setSubmenuList(List<Submenu> submenuList) {
        this.submenuList = submenuList;
    }

    public DualListModel<Submenu> getSubmenuDualList() {
        return submenuDualList;
    }

    public void setSubmenuDualList(DualListModel<Submenu> submenuDualList) {
        this.submenuDualList = submenuDualList;
    }

    public void logout() {
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();

        String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
        try {
            // Usar el contexto de JSF para invalidar la sesión,
            // NO EL DE SERVLETS (nada de HttpServletRequest)
            ((HttpSession) ctx.getSession(false)).invalidate();

           
            ctx.redirect(ctxPath + "/faces/login.xhtml");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
