/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.com.facultad.is2.model.Sprint;
import py.com.facultad.is2.model.SprintDet;
import py.com.facultad.is2.model.Userstory;

/**
 *
 * @author victoro
 */
@Stateless
public class SprintFacade extends AbstractFacade<Sprint> {

    @EJB
    private SprintDetFacade sprintDetEJB;

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void create(Sprint sprint, List<Userstory> usList) {
        getEntityManager().persist(sprint);
        List<SprintDet> sprintDetList = sprintDetEJB.obtenerListaSprintDetallePorSprint(sprint);
        List<Userstory> userStoryList = new ArrayList<>();
        List<Userstory> aAgregar = new ArrayList<>();
        List<Userstory> aEliminar = new ArrayList<>();
        for (SprintDet m : sprintDetList) {
            if (!userStoryList.contains(m.getIdUserstory())) {
                userStoryList.add(m.getIdUserstory());
            }
        }

        for (Userstory r : userStoryList) {
            if (!usList.contains(r)) {
                aEliminar.add(r);
            }
        }

        for (Userstory r : usList) {
            if (!userStoryList.contains(r)) {
                aAgregar.add(r);
            }
        }

        for (Userstory r : aEliminar) {
            SprintDet eliminar = sprintDetEJB.obtenerSprintDet(sprint, r);
            if (eliminar != null) {
                sprintDetEJB.remove(eliminar);
            }

        }

        for (Userstory r : aAgregar) {
            SprintDet rp = new SprintDet();
            rp.setIdUserstory(r);
            rp.setIdSprint(sprint);
            sprintDetEJB.create(rp);
        }
    }

    public void edit(Sprint sprint, List<Userstory> usList) {
        getEntityManager().merge(sprint);
        List<SprintDet> sprintDetList = sprintDetEJB.obtenerListaSprintDetallePorSprint(sprint);
        List<Userstory> userStoryList = new ArrayList<>();
        List<Userstory> aAgregar = new ArrayList<>();
        List<Userstory> aEliminar = new ArrayList<>();
        for (SprintDet m : sprintDetList) {
            if (!userStoryList.contains(m.getIdUserstory())) {
                userStoryList.add(m.getIdUserstory());
            }
        }

        for (Userstory r : userStoryList) {
            if (!usList.contains(r)) {
                aEliminar.add(r);
            }
        }

        for (Userstory r : usList) {
            if (!userStoryList.contains(r)) {
                aAgregar.add(r);
            }
        }

        for (Userstory r : aEliminar) {
            SprintDet eliminar = sprintDetEJB.obtenerSprintDet(sprint, r);
            if (eliminar != null) {
                sprintDetEJB.remove(eliminar);
            }

        }

        for (Userstory us : aAgregar) {
            SprintDet rp = new SprintDet();
            rp.setIdUserstory(us);
            rp.setIdSprint(sprint);
            sprintDetEJB.create(rp);
        }
    }

    public SprintFacade() {
        super(Sprint.class);
    }

}
