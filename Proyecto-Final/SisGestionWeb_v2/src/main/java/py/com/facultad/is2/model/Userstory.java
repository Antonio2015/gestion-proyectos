/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author victoro
 */
@Entity
@Table(name = "userstory")
@NamedQueries({
    @NamedQuery(name = "Userstory.findAll", query = "SELECT u FROM Userstory u")
    , @NamedQuery(name = "Userstory.findByIdUserstory", query = "SELECT u FROM Userstory u WHERE u.idUserstory = :idUserstory")
    , @NamedQuery(name = "Userstory.findByTiempoAsignado", query = "SELECT u FROM Userstory u WHERE u.tiempoAsignado = :tiempoAsignado")
    , @NamedQuery(name = "Userstory.findByDescripcion", query = "SELECT u FROM Userstory u WHERE u.descripcion = :descripcion")
    , @NamedQuery(name = "Userstory.findByValorNegocio", query = "SELECT u FROM Userstory u WHERE u.valorNegocio = :valorNegocio")
    , @NamedQuery(name = "Userstory.findByFechaInicio", query = "SELECT u FROM Userstory u WHERE u.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "Userstory.findByFechaFin", query = "SELECT u FROM Userstory u WHERE u.fechaFin = :fechaFin")})
public class Userstory implements Serializable {

    @JoinColumn(name = "id_estado", referencedColumnName = "id_estado")
    @ManyToOne
    private Estado idEstado;

    @OneToMany(mappedBy = "idUserstory")
    private List<SprintDet> sprintDetList;
    @JoinColumn(name = "id_backlog", referencedColumnName = "id_backlog")
    @ManyToOne
    private Backlog idBacklog;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_userstory", nullable = false)
    private Integer idUserstory;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre_largo", nullable = false, length = 2147483647)
    private String nombreLargo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "tiempo_asignado", nullable = false, length = 2147483647)
    private String tiempoAsignado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion", nullable = false, length = 2147483647)
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre_corto", nullable = false, length = 2147483647)
    private String nombreCorto;
    @Size(min = 1, max = 2147483647)
    @Column(name = "valor_negocio", nullable = false, length = 2147483647)
    private String valorNegocio;
    @Size(min = 1, max = 2147483647)
    @Column(name = "valor_tecnico", nullable = false, length = 2147483647)
    private String valorTecnico;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_fin", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario", nullable = false)
    @ManyToOne(optional = false)
    private Usuario idUsuario;

    public Userstory() {
    }

    public Userstory(Integer idUserstory) {
        this.idUserstory = idUserstory;
    }

    public Userstory(Integer idUserstory, String nombreLargo, String tiempoAsignado, String descripcion,
            String nombreCorto, String valorNegocio, Date fechaInicio, Date fechaFin) {
        this.idUserstory = idUserstory;
        this.nombreLargo = nombreLargo;
        this.tiempoAsignado = tiempoAsignado;
        this.descripcion = descripcion;
        this.nombreCorto = nombreCorto;
        this.valorNegocio = valorNegocio;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public Integer getIdUserstory() {
        return idUserstory;
    }

    public void setIdUserstory(Integer idUserstory) {
        this.idUserstory = idUserstory;
    }

    public String getNombreLargo() {
        return nombreLargo;
    }

    public void setNombreLargo(String nombreLargo) {
        this.nombreLargo = nombreLargo;
    }

    public String getTiempoAsignado() {
        return tiempoAsignado;
    }

    public void setTiempoAsignado(String tiempoAsignado) {
        this.tiempoAsignado = tiempoAsignado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getValorNegocio() {
        return valorNegocio;
    }

    public void setValorNegocio(String valorNegocio) {
        this.valorNegocio = valorNegocio;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
  
    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getValorTecnico() {
        return valorTecnico;
    }

    public void setValorTecnico(String valorTecnico) {
        this.valorTecnico = valorTecnico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUserstory != null ? idUserstory.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userstory)) {
            return false;
        }
        Userstory other = (Userstory) object;
        if ((this.idUserstory == null && other.idUserstory != null) || (this.idUserstory != null && !this.idUserstory.equals(other.idUserstory))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.facultad.is2.model.Userstory[ idUserstory=" + idUserstory + " ]";
    }

    public List<SprintDet> getSprintDetList() {
        return sprintDetList;
    }

    public void setSprintDetList(List<SprintDet> sprintDetList) {
        this.sprintDetList = sprintDetList;
    }

    public Backlog getIdBacklog() {
        return idBacklog;
    }

    public void setIdBacklog(Backlog idBacklog) {
        this.idBacklog = idBacklog;
    }

    public Estado getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estado idEstado) {
        this.idEstado = idEstado;
    }

}
