/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author victoro
 */
@Entity
@Table(name = "proyecto")
@NamedQueries({
    @NamedQuery(name = "Proyecto.findAll", query = "SELECT p FROM Proyecto p")
    , @NamedQuery(name = "Proyecto.findByIdProyecto", query = "SELECT p FROM Proyecto p WHERE p.idProyecto = :idProyecto")
    , @NamedQuery(name = "Proyecto.findByNombreProyecto", query = "SELECT p FROM Proyecto p WHERE p.nombreProyecto = :nombreProyecto")
    , @NamedQuery(name = "Proyecto.findByFechaFin", query = "SELECT p FROM Proyecto p WHERE p.fechaFin = :fechaFin")
    , @NamedQuery(name = "Proyecto.findByCantidadSprint", query = "SELECT p FROM Proyecto p WHERE p.cantidadSprint = :cantidadSprint")
    , @NamedQuery(name = "Proyecto.findByFechaFinEstimada", query = "SELECT p FROM Proyecto p WHERE p.fechaFinEstimada = :fechaFinEstimada")
    , @NamedQuery(name = "Proyecto.findByAnhoProyecto", query = "SELECT p FROM Proyecto p WHERE p.anhoProyecto = :anhoProyecto")})
public class Proyecto implements Serializable {

    @Column(name = "inactivo")
    private Boolean inactivo;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProyecto")
    private List<Backlog> backlogList;

    
 

    @Size(max = 30)
    @Column(name = "codigo", length = 30)
    private String codigo;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @OneToMany(mappedBy = "idProyecto")
    private List<ProyectoClientes> proyectoClientesList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proyecto", nullable = false)
    private Integer idProyecto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre_proyecto", nullable = false, length = 2147483647)
    private String nombreProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "cantidad_sprint", nullable = false, length = 2147483647)
    private String cantidadSprint;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_fin_estimada", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaFinEstimada;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "anho_proyecto", nullable = false, length = 2147483647)
    private String anhoProyecto;
    @JoinColumn(name = "id_equipo", referencedColumnName = "id_equipo", nullable = false)
    @ManyToOne(optional = false)
    private Equipo idEquipo;
    @JoinColumn(name = "id_estado", referencedColumnName = "id_estado", nullable = false)
    @ManyToOne(optional = false)
    private Estado idEstado;


    public Proyecto() {
    }

    public Proyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Proyecto(Integer idProyecto, String nombreProyecto, Date fechaFin, String cantidadSprint, String estadoProyecto, Date fechaFinEstimada, String anhoProyecto) {
        this.idProyecto = idProyecto;
        this.nombreProyecto = nombreProyecto;
        this.fechaFin = fechaFin;
        this.cantidadSprint = cantidadSprint;
        this.fechaFinEstimada = fechaFinEstimada;
        this.anhoProyecto = anhoProyecto;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getCantidadSprint() {
        return cantidadSprint;
    }

    public void setCantidadSprint(String cantidadSprint) {
        this.cantidadSprint = cantidadSprint;
    }

    public Date getFechaFinEstimada() {
        return fechaFinEstimada;
    }

    public void setFechaFinEstimada(Date fechaFinEstimada) {
        this.fechaFinEstimada = fechaFinEstimada;
    }

    public String getAnhoProyecto() {
        return anhoProyecto;
    }

    public void setAnhoProyecto(String anhoProyecto) {
        this.anhoProyecto = anhoProyecto;
    }
   
    public Equipo getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Equipo idEquipo) {
        this.idEquipo = idEquipo;
    }

    public Estado getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estado idEstado) {
        this.idEstado = idEstado;
    }

   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProyecto != null ? idProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto)) {
            return false;
        }
        Proyecto other = (Proyecto) object;
        if ((this.idProyecto == null && other.idProyecto != null) || (this.idProyecto != null && !this.idProyecto.equals(other.idProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.facultad.is2.model.Proyecto[ idProyecto=" + idProyecto + " ]";
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public List<ProyectoClientes> getProyectoClientesList() {
        return proyectoClientesList;
    }

    public void setProyectoClientesList(List<ProyectoClientes> proyectoClientesList) {
        this.proyectoClientesList = proyectoClientesList;
    }

    public List<Backlog> getBacklogList() {
        return backlogList;
    }

    public void setBacklogList(List<Backlog> backlogList) {
        this.backlogList = backlogList;
    }

    public Boolean getInactivo() {
        return inactivo;
    }

    public void setInactivo(Boolean inactivo) {
        this.inactivo = inactivo;
    }



}
