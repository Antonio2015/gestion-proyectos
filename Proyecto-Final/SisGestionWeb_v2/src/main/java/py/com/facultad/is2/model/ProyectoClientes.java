/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author victoro
 */
@Entity
@Table(name = "proyecto_clientes")
@NamedQueries({
    @NamedQuery(name = "ProyectoClientes.findAll", query = "SELECT p FROM ProyectoClientes p")
    , @NamedQuery(name = "ProyectoClientes.findByIdProyectoCliente", query = "SELECT p FROM ProyectoClientes p WHERE p.idProyectoCliente = :idProyectoCliente")})
public class ProyectoClientes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proyecto_cliente", nullable = false)
    private Integer idProyectoCliente;
    @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto")
    @ManyToOne
    private Proyecto idProyecto;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;

    public ProyectoClientes() {
    }

    public ProyectoClientes(Integer idProyectoCliente) {
        this.idProyectoCliente = idProyectoCliente;
    }

    public Integer getIdProyectoCliente() {
        return idProyectoCliente;
    }

    public void setIdProyectoCliente(Integer idProyectoCliente) {
        this.idProyectoCliente = idProyectoCliente;
    }

    public Proyecto getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Proyecto idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProyectoCliente != null ? idProyectoCliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProyectoClientes)) {
            return false;
        }
        ProyectoClientes other = (ProyectoClientes) object;
        if ((this.idProyectoCliente == null && other.idProyectoCliente != null) || (this.idProyectoCliente != null && !this.idProyectoCliente.equals(other.idProyectoCliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.facultad.is2.model.ProyectoClientes[ idProyectoCliente=" + idProyectoCliente + " ]";
    }
    
}
