/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;
import py.com.facultad.is2.facade.BacklogFacade;
import py.com.facultad.is2.facade.EstadoFacade;
import py.com.facultad.is2.facade.ProyectoFacade;
import py.com.facultad.is2.facade.SprintFacade;
import py.com.facultad.is2.facade.UserstoryFacade;
import py.com.facultad.is2.facade.UsuarioFacade;
import py.com.facultad.is2.model.Backlog;
import py.com.facultad.is2.model.Estado;
import py.com.facultad.is2.model.Sprint;
import py.com.facultad.is2.model.Userstory;
import py.com.facultad.is2.model.Usuario;

/**
 *
 * @author victoro
 */
@ManagedBean
@SessionScoped
public class UserStoryBean extends AbstractBean implements Serializable {

    private List<Userstory> userStoryList = new ArrayList<>();
    private List<Usuario> usuarioList = new ArrayList<>();
    private List<Sprint> sprintList = new ArrayList<>();
    private List<Backlog> backlogList = new ArrayList<>();
    private List<Estado> estadosList = new ArrayList<>();

    private Estado estadoSeleccionado;
    private Userstory userStorySeleccionado;
    private Usuario usuarioSeleccionado;
    private Sprint sprintSeleccionado;
    private Backlog backlogSeleccionado;

    private boolean editando;

    @EJB
    ProyectoFacade proyectoEJB;

    @EJB
    UsuarioFacade usuarioEJB;

    @EJB
    SprintFacade sprintEJB;

    @EJB
    UserstoryFacade userStoryEJB;

    @EJB
    BacklogFacade backlogEJB;

    @EJB
    EstadoFacade estadoEJB;

    @Override
    public void init() {
        userStorySeleccionado = new Userstory();
        userStoryList = userStoryEJB.findAll();
        sprintList = sprintEJB.findAll();
        backlogList = backlogEJB.findAll();
        usuarioList = usuarioEJB.findAll();
        estadosList = estadoEJB.findAll();

    }

    @Override
    public void resetearValores() {
        userStorySeleccionado = null;
        sprintSeleccionado = null;
        usuarioSeleccionado = null;
        backlogSeleccionado = null;
        userStorySeleccionado = new Userstory();
        estadoSeleccionado = null;
        editando = false;

    }

    public void agregarUS() {
        resetearValores();
        init();
    }

    @Override
    public void inicializarListas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void guardar() {
        try {
            userStorySeleccionado.setIdUsuario(usuarioSeleccionado);
            userStorySeleccionado.setIdBacklog(backlogSeleccionado);
            userStorySeleccionado.setIdEstado(estadoSeleccionado);
            userStoryEJB.create(userStorySeleccionado);
            infoMessage("Se guardó correctamente.");
            userStoryList = userStoryEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbUserStory').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void actualizar() {
        try {
            userStorySeleccionado.setIdUsuario(usuarioSeleccionado);
            userStorySeleccionado.setIdBacklog(backlogSeleccionado);
            userStorySeleccionado.setIdEstado(estadoSeleccionado);
            userStoryEJB.edit(userStorySeleccionado);
            infoMessage("Se actualizó correctamente.");
            userStoryList = userStoryEJB.findAll();
            resetearValores();
            RequestContext.getCurrentInstance().execute("PF('wbUserStory').hide()");
        } catch (Exception e) {
            errorMessage("Se produjo un error.");
        }
    }

    @Override
    public void eliminar() {
        try {
            userStoryEJB.remove(userStorySeleccionado);
            infoMessage("Eliminado correctamente");
            userStoryList = userStoryEJB.findAll();
        } catch (Exception e) {
            errorMessage("No se pudo eliminar el registro");
        }
    }

    @Override
    public void antesActualizar() {
        editando = true;
        estadoSeleccionado = userStorySeleccionado.getIdEstado();
        usuarioSeleccionado = userStorySeleccionado.getIdUsuario();
        backlogSeleccionado = userStorySeleccionado.getIdBacklog();
        editando = true;
        init();
    }

    public List<Userstory> getUserStoryList() {
        return userStoryList;
    }

    public void setUserStoryList(List<Userstory> userStoryList) {
        this.userStoryList = userStoryList;
    }

    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    public List<Sprint> getSprintList() {
        return sprintList;
    }

    public void setSprintList(List<Sprint> sprintList) {
        this.sprintList = sprintList;
    }

    public Userstory getUserStorySeleccionado() {
        return userStorySeleccionado;
    }

    public void setUserStorySeleccionado(Userstory userStorySeleccionado) {
        this.userStorySeleccionado = userStorySeleccionado;
    }

    public Usuario getUsuarioSeleccionado() {
        return usuarioSeleccionado;
    }

    public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
        this.usuarioSeleccionado = usuarioSeleccionado;
    }

    public Sprint getSprintSeleccionado() {
        return sprintSeleccionado;
    }

    public void setSprintSeleccionado(Sprint sprintSeleccionado) {
        this.sprintSeleccionado = sprintSeleccionado;
    }

    public boolean isEditando() {
        return editando;
    }

    public void setEditando(boolean editando) {
        this.editando = editando;
    }

    public List<Backlog> getBacklogList() {
        return backlogList;
    }

    public void setBacklogList(List<Backlog> backlogList) {
        this.backlogList = backlogList;
    }

    public Backlog getBacklogSeleccionado() {
        return backlogSeleccionado;
    }

    public void setBacklogSeleccionado(Backlog backlogSeleccionado) {
        this.backlogSeleccionado = backlogSeleccionado;
    }

    public List<Estado> getEstadosList() {
        return estadosList;
    }

    public void setEstadosList(List<Estado> estadosList) {
        this.estadosList = estadosList;
    }

    public Estado getEstadoSeleccionado() {
        return estadoSeleccionado;
    }

    public void setEstadoSeleccionado(Estado estadoSeleccionado) {
        this.estadoSeleccionado = estadoSeleccionado;
    }

}
