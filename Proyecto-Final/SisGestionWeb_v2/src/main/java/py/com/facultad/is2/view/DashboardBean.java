/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.event.DragDropEvent;
import py.com.facultad.is2.facade.EstadoFacade;
import py.com.facultad.is2.facade.SprintDetFacade;
import py.com.facultad.is2.facade.SprintFacade;
import py.com.facultad.is2.facade.UserstoryFacade;
import py.com.facultad.is2.model.Estado;
import py.com.facultad.is2.model.Sprint;
import py.com.facultad.is2.model.SprintDet;
import py.com.facultad.is2.model.Userstory;

/**
 *
 * @author victoro
 */
@ManagedBean
@SessionScoped
public class DashboardBean extends AbstractBean implements Serializable {

    private List<Sprint> sprintList = new ArrayList<>();
    private Sprint sprintSeleccionado;

    @EJB
    SprintFacade sprintEJB;

    @EJB
    SprintDetFacade sprintDetEJB;

    @EJB
    EstadoFacade estadosEJB;

    @EJB
    UserstoryFacade userStoryEJB;

    List<Userstory> usPendientes = new ArrayList<>();
    List<Userstory> usEnCurso = new ArrayList<>();
    List<Userstory> usTerminados = new ArrayList<>();

    @Override
    public void init() {
        sprintList = sprintEJB.findAll();
        sprintSeleccionado = null;
        resetearValores();
    }
    
    

    public static final int DEFAULT_COLUMN_COUNT = 3;
    private int columnCount = DEFAULT_COLUMN_COUNT;

    @Override
    public void resetearValores() {
        sprintSeleccionado = null;
        usPendientes = new ArrayList<>();
        usEnCurso = new ArrayList<>();
        usTerminados = new ArrayList<>();
    }

    @Override
    public void inicializarListas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void guardar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void antesActualizar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Sprint> getSprintList() {
        return sprintList;
    }

    public void setSprintList(List<Sprint> sprintList) {
        this.sprintList = sprintList;
    }

    public Sprint getSprintSeleccionado() {
        return sprintSeleccionado;
    }

    public void setSprintSeleccionado(Sprint sprintSeleccionado) {
        this.sprintSeleccionado = sprintSeleccionado;
    }

    public List<Userstory> getUsPendientes() {
        return usPendientes;
    }

    public void setUsPendientes(List<Userstory> usPendientes) {
        this.usPendientes = usPendientes;
    }

    public List<Userstory> getUsEnCurso() {
        return usEnCurso;
    }

    public void setUsEnCurso(List<Userstory> usEnCurso) {
        this.usEnCurso = usEnCurso;
    }

    public List<Userstory> getUsTerminados() {
        return usTerminados;
    }

    public void setUsTerminados(List<Userstory> usTerminados) {
        this.usTerminados = usTerminados;
    }

    public void generarDashboard() {
        List<SprintDet> sprintDetList = sprintDetEJB.cargarTareas(sprintSeleccionado);
        List<Estado> estadosList = estadosEJB.estadosSprint();

        usPendientes.clear();
        usEnCurso.clear();
        usTerminados.clear();

        List<SprintDet> sprintDetPendientes = sprintDetEJB.cargarTareasPendientes(sprintSeleccionado);
        List<SprintDet> sprintDetEnCurso = sprintDetEJB.cargarTareasEnCurso(sprintSeleccionado);
        List<SprintDet> sprintDetTerminados = sprintDetEJB.cargarTareasTerminadas(sprintSeleccionado);

        for (SprintDet pendientes : sprintDetPendientes) {
            usPendientes.add(pendientes.getIdUserstory());
        }

        for (SprintDet enCurso : sprintDetEnCurso) {
            usEnCurso.add(enCurso.getIdUserstory());
        }

        for (SprintDet terminados : sprintDetTerminados) {
            usTerminados.add(terminados.getIdUserstory());
        }
    }

    public void pendientesToEnCurso(DragDropEvent ddEvent) {
        Userstory us = (Userstory) ddEvent.getData();

        usEnCurso.add(us);
        usPendientes.remove(us);
        userStoryEJB.actualizarEstado(us, "CUR");
    }

    public void enCursoToPendientes(DragDropEvent ddEvent) {
        Userstory us = (Userstory) ddEvent.getData();

        usPendientes.add(us);
        usEnCurso.remove(us);
        userStoryEJB.actualizarEstado(us, "PEN");

    }

    public void enCursoToTerminados(DragDropEvent ddEvent) {
        Userstory us = (Userstory) ddEvent.getData();
        usTerminados.add(us);
        usEnCurso.remove(us);
        userStoryEJB.actualizarEstado(us, "TER");
    }

    public void terminadosToPendientes(DragDropEvent ddEvent) {
        Userstory us = (Userstory) ddEvent.getData();
        usPendientes.add(us);
        usTerminados.remove(us);
        userStoryEJB.actualizarEstado(us, "PEN");

    }
}
