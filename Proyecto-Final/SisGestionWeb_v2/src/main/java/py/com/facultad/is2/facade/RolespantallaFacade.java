/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import py.com.facultad.is2.model.Pantallas;
import py.com.facultad.is2.model.Roles;
import py.com.facultad.is2.model.Rolespantalla;

/**
 *
 * @author victoro
 */
@Stateless
public class RolespantallaFacade extends AbstractFacade<Rolespantalla> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RolespantallaFacade() {
        super(Rolespantalla.class);
    }

    public List<Rolespantalla> obtenerRolesPantalla(Integer idRol) {
        List<Rolespantalla> listaRoles = new ArrayList<>();
        try {
            Query q = em.createQuery("Select r from Rolespantalla r where r.idRol.idRol = :idRol");
            q.setParameter("idRol", idRol);
            listaRoles = q.getResultList();
        } catch (Exception e) {
            System.out.println("");
        }

        return listaRoles;
    }
    
     public List<Rolespantalla> obtenerListaRolesPantalla(Pantallas pantalla) {
        List<Rolespantalla> rolesPantallaList = new ArrayList<>();
        try {
            Query q = em.createQuery("Select r from Rolespantalla r where r.idPantalla = :pantalla")
                    .setParameter("pantalla", pantalla);
                    
            rolesPantallaList =  q.getResultList();
        } catch (Exception e) {

        }

        return rolesPantallaList;

    }
     
     public Rolespantalla obtenerRolesPantalla(Pantallas pantalla, Roles rol) {
        Rolespantalla m = null;
        try {
            Query q = em.createQuery("Select r from Rolespantalla r where r.idPantalla = :pantalla and r.idRol = :rol", Rolespantalla.class)
                    .setParameter("pantalla", pantalla)
                    .setParameter("rol", rol);
            m = (Rolespantalla) q.getSingleResult();
        } catch (Exception e) {

        }

        return m;

    }

}
