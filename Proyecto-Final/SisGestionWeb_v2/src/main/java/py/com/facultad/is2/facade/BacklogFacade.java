/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.facultad.is2.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.com.facultad.is2.model.Backlog;
import py.com.facultad.is2.model.Sprint;

/**
 *
 * @author victoro
 */
@Stateless
public class BacklogFacade extends AbstractFacade<Backlog> {

    @PersistenceContext(unitName = "SisGestionWeb_v2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BacklogFacade() {
        super(Backlog.class);
    }

    public List<Sprint> findByProyecto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
