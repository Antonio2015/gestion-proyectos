package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Proyecto;
import py.com.facultad.is2.model.Sprint;
import py.com.facultad.is2.model.Userstory;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Estado.class)
public class Estado_ { 

    public static volatile ListAttribute<Estado, Proyecto> proyectoList;
    public static volatile SingularAttribute<Estado, String> codigo;
    public static volatile SingularAttribute<Estado, Integer> idEstado;
    public static volatile ListAttribute<Estado, Userstory> userstoryList;
    public static volatile SingularAttribute<Estado, String> nombreEstado;
    public static volatile ListAttribute<Estado, Sprint> sprintList;

}