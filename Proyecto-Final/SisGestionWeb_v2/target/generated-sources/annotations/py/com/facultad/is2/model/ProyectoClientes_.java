package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Proyecto;
import py.com.facultad.is2.model.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(ProyectoClientes.class)
public class ProyectoClientes_ { 

    public static volatile SingularAttribute<ProyectoClientes, Proyecto> idProyecto;
    public static volatile SingularAttribute<ProyectoClientes, Integer> idProyectoCliente;
    public static volatile SingularAttribute<ProyectoClientes, Usuario> idUsuario;

}