package py.com.facultad.is2.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Backlog;
import py.com.facultad.is2.model.Estado;
import py.com.facultad.is2.model.SprintDet;
import py.com.facultad.is2.model.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Userstory.class)
public class Userstory_ { 

    public static volatile SingularAttribute<Userstory, String> descripcion;
    public static volatile SingularAttribute<Userstory, Usuario> idUsuario;
    public static volatile SingularAttribute<Userstory, Integer> idUserstory;
    public static volatile SingularAttribute<Userstory, Date> fechaFin;
    public static volatile SingularAttribute<Userstory, Backlog> idBacklog;
    public static volatile SingularAttribute<Userstory, String> tiempoAsignado;
    public static volatile SingularAttribute<Userstory, String> nombreCorto;
    public static volatile SingularAttribute<Userstory, String> valorNegocio;
    public static volatile SingularAttribute<Userstory, Estado> idEstado;
    public static volatile SingularAttribute<Userstory, String> nombreLargo;
    public static volatile ListAttribute<Userstory, SprintDet> sprintDetList;
    public static volatile SingularAttribute<Userstory, Date> fechaInicio;
    public static volatile SingularAttribute<Userstory, String> valorTecnico;

}