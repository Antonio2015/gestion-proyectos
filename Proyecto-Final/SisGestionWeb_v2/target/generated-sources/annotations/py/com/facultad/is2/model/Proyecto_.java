package py.com.facultad.is2.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Backlog;
import py.com.facultad.is2.model.Equipo;
import py.com.facultad.is2.model.Estado;
import py.com.facultad.is2.model.ProyectoClientes;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Proyecto.class)
public class Proyecto_ { 

    public static volatile SingularAttribute<Proyecto, Integer> idProyecto;
    public static volatile SingularAttribute<Proyecto, String> codigo;
    public static volatile SingularAttribute<Proyecto, String> cantidadSprint;
    public static volatile SingularAttribute<Proyecto, Equipo> idEquipo;
    public static volatile SingularAttribute<Proyecto, String> anhoProyecto;
    public static volatile SingularAttribute<Proyecto, String> nombreProyecto;
    public static volatile SingularAttribute<Proyecto, Boolean> inactivo;
    public static volatile SingularAttribute<Proyecto, Date> fechaFin;
    public static volatile ListAttribute<Proyecto, ProyectoClientes> proyectoClientesList;
    public static volatile SingularAttribute<Proyecto, Estado> idEstado;
    public static volatile ListAttribute<Proyecto, Backlog> backlogList;
    public static volatile SingularAttribute<Proyecto, Date> fechaInicio;
    public static volatile SingularAttribute<Proyecto, Date> fechaFinEstimada;

}