package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Sprint;
import py.com.facultad.is2.model.Userstory;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(SprintDet.class)
public class SprintDet_ { 

    public static volatile SingularAttribute<SprintDet, Integer> idSprintDet;
    public static volatile SingularAttribute<SprintDet, Sprint> idSprint;
    public static volatile SingularAttribute<SprintDet, Userstory> idUserstory;

}