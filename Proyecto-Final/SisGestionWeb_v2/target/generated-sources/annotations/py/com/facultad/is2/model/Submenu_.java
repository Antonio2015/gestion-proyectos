package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.MenuSubmenu;
import py.com.facultad.is2.model.Pantallas;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Submenu.class)
public class Submenu_ { 

    public static volatile SingularAttribute<Submenu, String> icono;
    public static volatile SingularAttribute<Submenu, Pantallas> idPantalla;
    public static volatile ListAttribute<Submenu, MenuSubmenu> menuSubmenuList;
    public static volatile SingularAttribute<Submenu, String> nombre;
    public static volatile SingularAttribute<Submenu, Integer> idSubmenu;

}