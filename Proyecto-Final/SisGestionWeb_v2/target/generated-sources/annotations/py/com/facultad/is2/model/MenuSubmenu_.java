package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Menu;
import py.com.facultad.is2.model.Submenu;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(MenuSubmenu.class)
public class MenuSubmenu_ { 

    public static volatile SingularAttribute<MenuSubmenu, Menu> idMenu;
    public static volatile SingularAttribute<MenuSubmenu, Integer> id;
    public static volatile SingularAttribute<MenuSubmenu, Submenu> idSubmenu;

}