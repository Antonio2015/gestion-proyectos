package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Proyecto;
import py.com.facultad.is2.model.Sprint;
import py.com.facultad.is2.model.Userstory;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Backlog.class)
public class Backlog_ { 

    public static volatile SingularAttribute<Backlog, String> descripcion;
    public static volatile SingularAttribute<Backlog, Proyecto> idProyecto;
    public static volatile SingularAttribute<Backlog, String> nombreBacklog;
    public static volatile ListAttribute<Backlog, Userstory> userstoryList;
    public static volatile ListAttribute<Backlog, Sprint> sprintList;
    public static volatile SingularAttribute<Backlog, Boolean> inactivo;
    public static volatile SingularAttribute<Backlog, Integer> idBacklog;

}