package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Permiso;
import py.com.facultad.is2.model.Rolespantalla;
import py.com.facultad.is2.model.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Roles.class)
public class Roles_ { 

    public static volatile SingularAttribute<Roles, Integer> idRol;
    public static volatile SingularAttribute<Roles, String> descripcionRol;
    public static volatile ListAttribute<Roles, Usuario> usuarioList;
    public static volatile SingularAttribute<Roles, String> nombreRol;
    public static volatile ListAttribute<Roles, Rolespantalla> rolespantallaList;
    public static volatile ListAttribute<Roles, Permiso> permisoList;

}