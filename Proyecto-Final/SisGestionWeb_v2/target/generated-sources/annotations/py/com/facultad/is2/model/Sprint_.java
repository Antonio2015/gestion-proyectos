package py.com.facultad.is2.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Backlog;
import py.com.facultad.is2.model.Estado;
import py.com.facultad.is2.model.SprintDet;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Sprint.class)
public class Sprint_ { 

    public static volatile SingularAttribute<Sprint, Estado> idEstado;
    public static volatile ListAttribute<Sprint, SprintDet> sprintDetList;
    public static volatile SingularAttribute<Sprint, Date> fechaInicio;
    public static volatile SingularAttribute<Sprint, Integer> idSprint;
    public static volatile SingularAttribute<Sprint, String> duracion;
    public static volatile SingularAttribute<Sprint, Boolean> inactivo;
    public static volatile SingularAttribute<Sprint, String> nombre;
    public static volatile SingularAttribute<Sprint, Date> fechaFin;
    public static volatile SingularAttribute<Sprint, String> observacion;
    public static volatile SingularAttribute<Sprint, Backlog> idBacklog;

}