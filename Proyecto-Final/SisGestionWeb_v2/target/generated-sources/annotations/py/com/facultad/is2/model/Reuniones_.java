package py.com.facultad.is2.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Equipo;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Reuniones.class)
public class Reuniones_ { 

    public static volatile SingularAttribute<Reuniones, Integer> idReunion;
    public static volatile SingularAttribute<Reuniones, Equipo> idEquipo;
    public static volatile SingularAttribute<Reuniones, Date> fechaReunion;
    public static volatile SingularAttribute<Reuniones, String> observacion;

}