package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Rolespantalla;
import py.com.facultad.is2.model.Submenu;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Pantallas.class)
public class Pantallas_ { 

    public static volatile ListAttribute<Pantallas, Submenu> submenuList;
    public static volatile SingularAttribute<Pantallas, Integer> idPantalla;
    public static volatile ListAttribute<Pantallas, Rolespantalla> rolespantallaList;
    public static volatile SingularAttribute<Pantallas, String> nombre;
    public static volatile SingularAttribute<Pantallas, String> url;

}