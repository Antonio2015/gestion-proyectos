package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Pantallas;
import py.com.facultad.is2.model.Roles;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Rolespantalla.class)
public class Rolespantalla_ { 

    public static volatile SingularAttribute<Rolespantalla, Roles> idRol;
    public static volatile SingularAttribute<Rolespantalla, Pantallas> idPantalla;
    public static volatile SingularAttribute<Rolespantalla, Integer> idRolesPantalla;

}