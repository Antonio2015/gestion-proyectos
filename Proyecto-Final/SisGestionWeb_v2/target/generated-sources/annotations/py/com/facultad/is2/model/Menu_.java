package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.MenuSubmenu;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Menu.class)
public class Menu_ { 

    public static volatile SingularAttribute<Menu, Integer> idMenu;
    public static volatile ListAttribute<Menu, MenuSubmenu> menuSubmenuList;
    public static volatile SingularAttribute<Menu, String> nombre;

}