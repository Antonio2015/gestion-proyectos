package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Proyecto;
import py.com.facultad.is2.model.Reuniones;
import py.com.facultad.is2.model.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Equipo.class)
public class Equipo_ { 

    public static volatile ListAttribute<Equipo, Proyecto> proyectoList;
    public static volatile SingularAttribute<Equipo, Integer> idEquipo;
    public static volatile SingularAttribute<Equipo, String> rolEquipo;
    public static volatile SingularAttribute<Equipo, String> nombreEquipo;
    public static volatile SingularAttribute<Equipo, Usuario> idUsuario;
    public static volatile ListAttribute<Equipo, Reuniones> reunionesList;

}