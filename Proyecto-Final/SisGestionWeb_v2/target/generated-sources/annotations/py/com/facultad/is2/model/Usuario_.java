package py.com.facultad.is2.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import py.com.facultad.is2.model.Equipo;
import py.com.facultad.is2.model.ProyectoClientes;
import py.com.facultad.is2.model.Roles;
import py.com.facultad.is2.model.Userstory;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-07-01T17:21:11")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, Roles> idRol;
    public static volatile ListAttribute<Usuario, Userstory> userstoryList;
    public static volatile SingularAttribute<Usuario, String> cedula;
    public static volatile SingularAttribute<Usuario, Boolean> esCliente;
    public static volatile SingularAttribute<Usuario, Integer> idUsuario;
    public static volatile SingularAttribute<Usuario, String> nombreUsuario;
    public static volatile SingularAttribute<Usuario, String> nombre;
    public static volatile SingularAttribute<Usuario, String> nombreEmpresa;
    public static volatile SingularAttribute<Usuario, String> direccionn;
    public static volatile ListAttribute<Usuario, ProyectoClientes> proyectoClientesList;
    public static volatile SingularAttribute<Usuario, String> password;
    public static volatile SingularAttribute<Usuario, String> apellido;
    public static volatile ListAttribute<Usuario, Equipo> equipoList;
    public static volatile SingularAttribute<Usuario, String> telefono;
    public static volatile SingularAttribute<Usuario, String> email;

}